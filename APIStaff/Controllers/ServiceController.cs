﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APIStaff.Extentions;
using APIStaff.Repository.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace APIStaff.Controllers
{
    [Produces("application/json")]
    [Route("api/service")]
    public class ServiceController : Controller
    {

        private ILogger<ServiceController> Logger { get; }
        private readonly IPushNotice PushNotice;
        private readonly IServiceRepo ServiceRepo;
        private readonly IFavoriteProductServiceRepo FavoriteProductServiceRepo;
        private readonly IStaffRepo StaffRepo;
        //constructor
        public ServiceController(ILogger<ServiceController> logger,
                                 IPushNotice pushNotice,
                                 IServiceRepo serviceRepo,
                                  IFavoriteProductServiceRepo favoriteProductServiceRepo,
                                 IStaffRepo staffRepo)
        {
            Logger = logger;
            PushNotice = pushNotice;
            ServiceRepo = serviceRepo;
            FavoriteProductServiceRepo = favoriteProductServiceRepo;
            StaffRepo = staffRepo;
            FavoriteProductServiceRepo = favoriteProductServiceRepo;
            StaffRepo = staffRepo;
        }

        /// <summary>
        /// Get all Service
        /// </summary>
        /// <returns></returns>
        [HttpGet("all")]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var data = await ServiceRepo.GetAll();
                return Ok(data);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", Inner: " + e.StackTrace);
                return StatusCode(500, new { message = e.Message });
            }
        }

        /// <summary>
        /// Get favorite Service
        /// </summary>
        /// <returns></returns>
        [HttpGet("favorite")]
        public async Task<IActionResult> GetFavoriteService([FromQuery] int staffId)
        {
            try
            {
                if (staffId <= 0)
                {
                    return BadRequest(new { message = "StaffId not exist!" });
                }

                var staff = await StaffRepo.Get(s => s.Id == staffId && s.IsDelete == 0 && s.Active == 1);
                if (staff == null)
                {
                    return NotFound(new { message = "Staff not found!" });
                }
                var data = await FavoriteProductServiceRepo.GetFavoriteService(staffId);
                return Ok(data);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", Inner: " + e.StackTrace);
                return StatusCode(500, new { message = e.Message });
            }
        }
    }
}
