﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APIStaff.Extentions;
using APIStaff.Repository.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace APIStaff.Controllers
{
    [Produces("application/json")]
    [Route("api/hairstyle")]
    public class HairStyleController : Controller
    {
        private ILogger<BillServiceController> Logger { get; }
        private readonly IPushNotice PushNotice;

        private readonly IHairStyleRepo HairStyleRepo;

        public HairStyleController(ILogger<BillServiceController> logger, IPushNotice pushNotice,
            IHairStyleRepo hairStyleRepo)
        {
            Logger = logger;
            PushNotice = pushNotice;
            HairStyleRepo = hairStyleRepo;
        }


        /// <summary>
        /// Get hair style
        /// </summary>
        /// <returns></returns>
        [HttpGet("index")]
        public async Task<IActionResult> GetHairStyle()
        {
            try
            {
                var list = await HairStyleRepo.GetHairStyle();
                return Ok(list);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", StackTrace: " + e.StackTrace);
                return StatusCode(500, new { message = e.Message });
            }
        }
    }
}