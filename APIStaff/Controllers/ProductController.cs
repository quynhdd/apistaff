﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APIStaff.Extentions;
using APIStaff.Repository.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace APIStaff.Controllers
{
    [Produces("application/json")]
    [Route("api/product")]
    public class ProductController : Controller
    {
        private ILogger<ProductController> Logger { get; }
        private readonly IPushNotice PushNotice;
        private readonly IProductRepo ProductRepo;
        private readonly IFavoriteProductServiceRepo FavoriteProductServiceRepo;
        private readonly IStaffRepo StaffRepo;

        public ProductController(ILogger<ProductController> logger,
                                 IPushNotice pushNotice,
                                 IProductRepo productRepo,
                                 IFavoriteProductServiceRepo favoriteProductServiceRepo,
                                 IStaffRepo staffRepo)
        {
            Logger = logger;
            PushNotice = pushNotice;
            ProductRepo = productRepo;
            FavoriteProductServiceRepo = favoriteProductServiceRepo;
            StaffRepo = staffRepo;
        }

        /// <summary>
        /// Get all Product
        /// </summary>
        /// <returns></returns>
        [HttpGet("all")]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var data = await ProductRepo.GetAll();
                return Ok(data);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", Inner: " + e.StackTrace);
                return StatusCode(500, new { message = e.Message });
            }
        }

        /// <summary>
        /// Get favorite Product
        /// </summary>
        /// <returns></returns>
        [HttpGet("favorite")]
        public async Task<IActionResult> GetFavoriteProduct([FromQuery] int staffId)
        {
            try
            {
                if (staffId <= 0)
                {
                    return BadRequest(new { message = "StaffId not exist!" });
                }

                var staff = await StaffRepo.Get(s => s.Id == staffId && s.IsDelete == 0 && s.Active == 1);
                if (staff == null)
                {
                    return NotFound(new { message = "Staff not found!" });
                }
                var data = await FavoriteProductServiceRepo.GetFavoriteProduct(staffId);
                return Ok(data);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", Inner: " + e.StackTrace);
                return StatusCode(500, new { message = e.Message });
            }
        }
    }
}
