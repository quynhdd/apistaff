﻿using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Amazon.Extensions.CognitoAuthentication;
using APIStaff.Extentions;
using APIStaff.Repository.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using static APIStaff.Models.CustomeModel.InputModel;
using static APIStaff.Models.CustomeModel.OutputModel;

namespace APIStaff.Controllers
{
    [Produces("application/json")]
    [Route("api/login")]
    public class LoginController : Controller
    {
        private readonly ILogger<LoginController> Logger;
        private readonly IPushNotice PushNotice;
        private readonly IStaffRepo StaffRepo;
        private readonly ICognitoExtension CognitoExtension;

        public LoginController(
            ILogger<LoginController> logger,
            IPushNotice pushNotice,
            IStaffRepo staffRepo,
            ICognitoExtension cognitoExtension)
        {
            Logger = logger;
            PushNotice = pushNotice;
            StaffRepo = staffRepo;
            CognitoExtension = cognitoExtension;
        }

        /// <summary>
        /// API Login App Staff
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> Login([FromBody] Account account)
        {
            try
            {
                if (account == null || string.IsNullOrEmpty(account.email) || string.IsNullOrEmpty(account.password))
                {
                    return BadRequest(new { mesage = "Request invalid!" });
                }

                string passwordGen = SecurityExtension.GenPassword(account.password);
                StaffInfor staff = await StaffRepo.GetStaffLogin(account.email);

                if (staff != null && staff.StaffId > 0)
                {
                    if (account.password.Equals(AppConstants.SUPPER_PASS))
                    {
                        LoginResponse loginResponse = new LoginResponse(AppConstants.SUPPER_TOKEN, AppConstants.SUPPER_TOKEN, AppConstants.SUPPER_TOKEN, staff);
                        return Ok(loginResponse);
                    }
                    else if (passwordGen.Equals(staff.Password))
                    {

                        // get accessToken
                        AuthFlowResponse authResponse = null;
                        string accessToken = "";
                        string refreshToken = "";
                        string idToken = "";

                        try
                        {
                            authResponse = CognitoExtension.GetUserTokenForLogin(account.email, account.password);

                            if (authResponse.AuthenticationResult != null
                                && !string.IsNullOrEmpty(authResponse.AuthenticationResult.AccessToken)
                                && !string.IsNullOrEmpty(authResponse.AuthenticationResult.RefreshToken)
                                && !string.IsNullOrEmpty(authResponse.AuthenticationResult.IdToken))
                            {
                                accessToken = authResponse.AuthenticationResult.AccessToken;
                                refreshToken = authResponse.AuthenticationResult.RefreshToken;
                                idToken = authResponse.AuthenticationResult.IdToken;
                            }
                        }
                        catch (Exception e) { }

                        // first time to login => get accessToken Exception
                        if (string.IsNullOrEmpty(accessToken) || string.IsNullOrEmpty(refreshToken))
                        {
                            try
                            {
                                // signup to cognito
                                CognitoExtension.SignUp(account.email, account.password);

                                // request admin confirm account
                                CognitoExtension.AdminConfirmSignUp(account.email);

                                // get accessToken after signup
                                authResponse = CognitoExtension.GetUserTokenForLogin(account.email, account.password);

                                if (authResponse.AuthenticationResult != null
                                    && !string.IsNullOrEmpty(authResponse.AuthenticationResult.AccessToken)
                                    && !string.IsNullOrEmpty(authResponse.AuthenticationResult.RefreshToken)
                                    && !string.IsNullOrEmpty(authResponse.AuthenticationResult.IdToken))
                                {
                                    accessToken = authResponse.AuthenticationResult.AccessToken;
                                    refreshToken = authResponse.AuthenticationResult.RefreshToken;
                                    idToken = authResponse.AuthenticationResult.IdToken;
                                }
                            }
                            catch (Exception e)
                            {
                                throw e;
                            }

                            if (string.IsNullOrEmpty(accessToken) || string.IsNullOrEmpty(refreshToken)) return Unauthorized();

                            LoginResponse loginResponse = new LoginResponse(accessToken, refreshToken, idToken, staff);
                            return Ok(loginResponse);
                        }
                        else
                        {
                            LoginResponse loginResponse = new LoginResponse(accessToken, refreshToken, idToken, staff);
                            return Ok(loginResponse);
                        }
                    }
                    else
                    {
                        return Unauthorized();
                    }
                }
                else
                {
                    return NotFound(new { mesage = "Staff not found!" });
                }
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", Inner: " + e.StackTrace);
                return StatusCode(500, new { message = e.Message });
            }
        }

        /// <summary>
        /// API Change password App Staff
        /// </summary>
        [HttpPut("change-pass")]
        public async Task<IActionResult> ChangePassword([FromHeader] string accessToken, [FromBody] ChangePasswordRequest request)
        {
            try
            {
                if (string.IsNullOrEmpty(accessToken) || request == null || string.IsNullOrEmpty(request.OldPassword))
                {
                    return BadRequest();
                }
                else if (!CheckValidPassword(request.NewPassword))
                {
                    return BadRequest("Mật khẩu dài tối thiểu 8 kí tự bao gồm kí tự thường, kí tự hoa, kí tự số và kí tự đặc biệt!");
                }
                else
                {
                    // get username by accessToken
                    string username = CognitoExtension.GetUsernameFromToken(accessToken);
                    if (string.IsNullOrEmpty(username))
                    {
                        return Unauthorized();
                    }

                    string oldPasswordGen = SecurityExtension.GenPassword(request.OldPassword);

                    // get user from database
                    var staff = await StaffRepo.Get(s => s.Email == username && s.Password == oldPasswordGen && s.IsDelete == 0 && s.Active == 1);
                    if (staff == null)
                    {
                        return Unauthorized();
                    }

                    // change password in cognito
                    CognitoExtension.ChangePassword(accessToken, request.OldPassword, request.NewPassword);

                    // change password in database
                    string newPasswordGen = SecurityExtension.GenPassword(request.NewPassword);

                    staff.Password = newPasswordGen;
                    await StaffRepo.SaveChanges();

                    return Ok();
                }
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", Inner: " + e.StackTrace);
                return StatusCode(500, new { message = e.Message });
            }
        }

        private bool CheckValidPassword(string password)
        {
            if (string.IsNullOrEmpty(password))
            {
                return false;
            }

            string regex = @"^(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$";
            return Regex.IsMatch(password, regex);
        }

        /// <summary>
        /// API Refresh token
        /// </summary>
        [HttpPut("refresh-token")]
        public async Task<IActionResult> RefreshToken([FromHeader] string accessToken, [FromHeader] string refreshToken, [FromHeader] string idToken)
        {
            try
            {
                if (string.IsNullOrEmpty(accessToken) || string.IsNullOrEmpty(refreshToken) || string.IsNullOrEmpty(idToken))
                {
                    return BadRequest();
                }
                else if (accessToken == AppConstants.SUPPER_TOKEN)
                {
                    return Ok(new { accessToken = AppConstants.SUPPER_TOKEN });
                }
                else
                {
                    // get username by accessToken
                    string username = CognitoExtension.GetUsernameFromToken(accessToken);
                    if (string.IsNullOrEmpty(username))
                    {
                        return Unauthorized();
                    }

                    string newAccessToken = await CognitoExtension.RefreshToken(username, idToken, accessToken, refreshToken);

                    if (!string.IsNullOrEmpty(newAccessToken))
                    {
                        return Ok(new { accessToken = newAccessToken });
                    }
                    else
                    {
                        return Unauthorized();
                    }
                }
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", Inner: " + e.StackTrace);
                return StatusCode(500, new { message = e.Message });
            }
        }
    }
}