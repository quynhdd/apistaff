﻿using System;
using System.Threading.Tasks;
using APIStaff.Extentions;
using APIStaff.Repository.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace APIStaff.Controllers
{
    [Produces("application/json")]
    [Route("api/staffbook")]
    public class StaffBookController : Controller
    {
        private ILogger<BillServiceController> Logger { get; }
        private readonly IPushNotice PushNotice;

        private readonly IStaffRepo StaffRepo;
        private readonly IStaffBookRepo StaffBookRepo;

        public StaffBookController(ILogger<BillServiceController> logger, IPushNotice pushNotice,
            IStaffRepo staffRepo, IStaffBookRepo staffBookRepo)
        {
            Logger = logger;
            PushNotice = pushNotice;
            StaffBookRepo = staffBookRepo;
            StaffRepo = staffRepo;
        }

        /// <summary>
        /// Get all StaffBook for Staff
        /// </summary>
        /// <param name="staffId">Id of staff</param>
        /// <returns></returns>
        [HttpGet("procedure")]
        public async Task<IActionResult> GetProcedure([FromQuery] int staffId)
        {
            try
            {
                if (staffId <= 0)
                {
                    return BadRequest(new { message = "StaffId must greater than 0!" });
                }
                var staff = await StaffRepo.Get(s => s.Id == staffId && s.IsDelete == 0 && s.Active == 1);
                if (staff == null || staff.Type == null)
                {
                    return BadRequest(new { message = "Staff not found!" });
                }

                var list = await StaffBookRepo.GetStaffBook(staff.Type);
                return Ok(list);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", StackTrace: " + e.StackTrace);
                return StatusCode(500, new { message = e.Message });
            }
        }
    }
}