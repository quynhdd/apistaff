﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APIStaff.Extentions;
using APIStaff.Repository.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace APIStaff.Controllers
{
    [Produces("application/json")]
    [Route("api/Customer")]
    public class CustomerController : Controller
    {
        private ILogger<CustomerController> Logger { get; }
        private readonly IPushNotice PushNotice;
        private readonly ICustomerRepo CustomerRepo;
        // constructor
        public CustomerController(ILogger<CustomerController> logger, IPushNotice pushNotice, ICustomerRepo customerRepo)
        {
            Logger = logger;
            PushNotice = pushNotice;
            CustomerRepo = customerRepo;
        }

        /// <summary>
        /// Get info special customer
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetSpecialCustomer([FromQuery] int customerId)
        {
            try
            {
                if (customerId <= 0)
                {
                    return BadRequest();
                }
                var data = await CustomerRepo.GetInfoSpecialCustomer(customerId);
                //
                if (data == null)
                {
                    return NoContent();
                }
                //
                return Ok(data);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", StackTrace: " + e.StackTrace);
                return StatusCode(500, new { message = e.Message });
            }
        }
    }
}