﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APIStaff.Extentions;
using APIStaff.Models.Solution_30Shine;
using APIStaff.Repository.Interface;
using APIStaff.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using static APIStaff.Models.CustomeModel.InputModel;

namespace APIStaff.Controllers
{
    [Produces("application/json")]
    [Route("api/checkin")]
    public class CheckinController : Controller
    {
        private ILogger<BillServiceController> Logger { get; }
        private readonly IPushNotice PushNotice;
        private readonly CheckinCheckoutService CheckinCheckoutService;

        private readonly ICheckinCheckoutRepo CheckinCheckoutRepo;
        private readonly IDeviceRepo DeviceRepo;
        private readonly IFlowTimeKeepingRepo FlowTimeKeepingRepo;
        private readonly IWorkTimeRepo WorkTimeRepo;

        public CheckinController(ILogger<BillServiceController> logger, IPushNotice pushNotice,
            ICheckinCheckoutRepo checkinCheckoutRepo, IDeviceRepo deviceRepo,
            IFlowTimeKeepingRepo flowTimeKeepingRepo, IWorkTimeRepo workTimeRepo,
            CheckinCheckoutService checkinCheckoutService)
        {
            Logger = logger;
            PushNotice = pushNotice;
            CheckinCheckoutRepo = checkinCheckoutRepo;
            DeviceRepo = deviceRepo;
            FlowTimeKeepingRepo = flowTimeKeepingRepo;
            WorkTimeRepo = workTimeRepo;
            CheckinCheckoutService = checkinCheckoutService;
        }

        /// <summary>
        /// Check checkin checkout status of staff today
        /// </summary>
        /// <param name="staffId">Id of staff</param>
        /// <returns>
        /// Staff not checkin : False
        /// Staff checkin but not checkout : True
        /// Staff has checkin and checkout : False
        /// </returns>
        [HttpGet("checkin-status")]
        public async Task<IActionResult> CheckCheckInStatus([FromQuery] int staffId)
        {
            try
            {
                if (staffId <= 0)
                {
                    return BadRequest(new { mesage = "StaffId must greater than 0!" });
                }
                bool isCheckin = await CheckinCheckoutRepo.CheckCheckinStatus(staffId);
                return Ok(new { status = isCheckin });
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", StackTrace: " + e.StackTrace);
                return StatusCode(500, new { message = e.Message });
            }
        }

        /// <summary>
        /// Checkin checkout for staff in current day
        /// </summary>
        /// Sample request:
        ///     {
        ///         "StaffId" : 123,
        ///         "CheckinCheckoutDatetime" : string,
        ///         "SalonId" : 24,
        ///         "Type" : 2,     // 1: Checkin ; 2: Checkout
        ///         "Imei" : string,
        ///         "UniqueKey" : string
        ///     }
        /// <returns>
        /// </returns>
        [HttpPost("staff-checkin-checkout")]
        public async Task<IActionResult> CheckinOrCheckoutStaff([FromBody] RequestCheckinCheckout request)
        {
            try
            {
                if (request == null || request.SalonId <= 0
                    || request.UniqueKey == "" || request.StaffId <= 0
                    || (request.Type != 1 && request.Type != 2)
                    || string.IsNullOrEmpty(request.CheckinCheckoutDatetime))
                {
                    return BadRequest(new { message = "Request invalid!" });
                }

                var device = await DeviceRepo.GetDevice(request.Imei, request.SalonId);
                if (device == null)
                {
                    return BadRequest(new { message = "Thiết bị chấm công không đúng!" });
                }
                DateTime checkInOutDate = DateTime.Parse(request.CheckinCheckoutDatetime);
                if (checkInOutDate <= DateTime.Now.AddDays(-1))
                {
                    throw new Exception();
                }

                var flowTimeKeeping = await FlowTimeKeepingRepo.Get(w => w.IsEnroll == true && w.StaffId == request.StaffId && w.SalonId == request.SalonId && w.WorkDate == checkInOutDate);
                if (flowTimeKeeping == null || flowTimeKeeping.WorkTimeId == null || flowTimeKeeping.WorkTimeId <= 0)
                {
                    return BadRequest(new { message = "Bạn chưa được chấm công!" });
                }
                var workTime = await WorkTimeRepo.GetById((int)flowTimeKeeping.WorkTimeId);
                if (workTime == null)
                {
                    throw new Exception();
                }

                if (request.Type == 1) // Checkin
                {
                    var checkinCheckoutRecord = await CheckinCheckoutRepo.Get(a => a.UniqueKey == request.UniqueKey && a.IsDelete == false);
                    if (checkinCheckoutRecord == null)
                    {
                        CheckinCheckout record = new CheckinCheckout();
                        record.CheckinTime = checkInOutDate;
                        record.SalonId = request.SalonId;
                        record.StaffId = request.StaffId;
                        record.UniqueKey = request.UniqueKey;
                        record.WorkDate = checkInOutDate.Date;
                        record.FlowTimeKeepingId = flowTimeKeeping.Id;
                        record.IsDelete = false;

                        await CheckinCheckoutService.AddCheckinTime(record, flowTimeKeeping, workTime.StrartTime);
                    }
                    else
                    {
                        throw new Exception();
                    }
                    return Ok(new { message = "Checkin thành công!" });
                }
                else // Checkout
                {
                    await CheckinCheckoutService.AddCheckoutTime(request.StaffId, request.SalonId, checkInOutDate, flowTimeKeeping, workTime.EnadTime);
                    return Ok(new { message = "Checkout thành công!" });
                }
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", StackTrace: " + e.StackTrace);
                return StatusCode(500, new { message = e.Message });
            }
        }
    }
}