﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APIStaff.Extentions;
using APIStaff.Repository.Interface;
using APIStaff.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using APIStaff.Models.CustomeModel;
using static APIStaff.Models.CustomeModel.OutputModel;
using Bill = APIStaff.Models.Solution_30Shine.BillService;

namespace APIStaff.Controllers
{
    [Produces("application/json")]
    [Route("api/billservice")]
    public class BillServiceController : Controller
    {
        private ILogger<BillServiceController> Logger { get; }
        private readonly IPushNotice PushNotice;
        private readonly BillService BillService;
        private readonly IBillServiceRepo BillServiceRepo;
        private readonly IBillServiceHisRepo BillServiceHisRepo;
        private readonly IStaffRepo StaffRepo;
        //constructor
        public BillServiceController(ILogger<BillServiceController> logger,
                                     IPushNotice pushNotice,
                                     BillService billService,
                                     IBillServiceRepo billServiceRepo,
                                     IBillServiceHisRepo billServiceHisRepo,
                                     IStaffRepo staffRepo)
        {
            Logger = logger;
            PushNotice = pushNotice;
            BillService = billService;
            BillServiceRepo = billServiceRepo;
            BillServiceHisRepo = billServiceHisRepo;
            StaffRepo = staffRepo;
        }


        /// <summary>
        /// check history customer
        /// </summary>
        /// <param name="billId"></param>
        /// <returns></returns>
        [HttpGet("history-customer-billservice")]
        public async Task<IActionResult> GetHistoryCustomerBillSerivce([FromQuery] int CustomerId)
        {
            try
            {
                if (CustomerId <= 0)
                {
                    return BadRequest(new { Message = "Data does not exist!" });
                }
                var data = await BillService.GetHistoryBillService(CustomerId);
                return Ok(data);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", Inner: " + e.StackTrace);
                return StatusCode(500, new { message = e.Message });
            }
        }

        [HttpGet("history")]
        public async Task<IActionResult> GetLatestBills([FromQuery] int customerId, int billCount = 5)
        {
            try
            {
                if (customerId <= 0) return BadRequest("customerId not valid.");
                List<CustomerBill> customerBills = await BillServiceHisRepo.GetLatestCustomerBills(customerId, billCount);
                if (customerBills.Count == 0) return NotFound("Customer bills not found.");
                return Ok(customerBills);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", Inner: " + e.StackTrace);
                return StatusCode(500, new { message = e.Message });
            }
        }

        // check skinner input orderCode greater than 12 minute or not
        private static double MINIMUM_WAIT_TIME = 12;
        [HttpGet("check-waittime")]
        public async Task<IActionResult> CheckWaitTimeGreaterThanTwelveMinutes([FromQuery] int billId, [FromQuery] int skinnerId)
        {
            try
            {
                Bill bill = await BillServiceRepo.GetDbWrite(b => b.Id == billId);
                if (bill is null) return NotFound("Bill is not found!");

                Tuple<double, bool> tmp = await BillServiceRepo.GetWaitTimeSinceLastService(skinnerId);

                if (tmp == null || !tmp.Item2 || tmp.Item1 >= MINIMUM_WAIT_TIME || bill.StaffHairMassageId == skinnerId)
                {
                    return Ok(new { greaterThanTwelveMinute = true });
                }
                else
                {
                    return Ok(new { greaterThanTwelveMinute = false });
                }
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", StackTrace: " + e.StackTrace);
                return StatusCode(500, new { message = e.Message });
            }
        }

        [HttpPut("order")]
        public async Task<IActionResult> UpdateOrderCode([FromQuery] int billId, [FromBody] InputModel.UpdateBillBody body)
        {
            try
            {
                int stylistId = body.StylistId;
                int skinnerId = body.SkinnerId;

                Bill bill = await BillServiceRepo.GetDbWrite(b => b.Id == billId);
                if (bill is null) return NotFound("Bill does not exist");

                bill.StaffHairdresserId = stylistId;
                bill.StaffHairMassageId = skinnerId;

                if (bill.InProcedureTime == null)
                {
                    bill.InProcedureTime = DateTime.Now;
                }
                else if (bill.StaffHairMassageId != skinnerId)
                {
                    bill.InProcedureTimeModifed = DateTime.Now;
                }

                bill.IsSulphite = false;

                // note which skinner input when she input orderCode in less than 12 minutes
                // save in CustomerCode1 field
                if (!string.IsNullOrEmpty(body.NoteLessThanMinutes))
                {
                    bill.CustomerCode1 = body.NoteLessThanMinutes;
                }
                bill.ModifiedDate = DateTime.Now;
                bool result = await BillServiceRepo.Update(bill);
                if (!result)
                {
                    return StatusCode(500, "Update failed. Please try again.");
                }
                return Ok(new { success = true });
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", StackTrace: " + e.StackTrace);
                return StatusCode(500, new { message = e.Message });
            }
        }

        /// <summary>
        /// check customer confirm
        /// </summary>
        /// <param name="billId"></param>
        /// <returns></returns>
        [HttpGet("offer-sale-for-staff")]
        public async Task<IActionResult> GetOfferSaleForCustomer([FromQuery] int CustomerId, int BillId)
        {
            try
            {
                if (CustomerId <= 0 || BillId <= 0)
                {
                    return BadRequest(new { Message = "Data does not exist!" });
                }
                var data = await BillService.GetOfferForCustomer(CustomerId, BillId);
                return Ok(data);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", Inner: " + e.StackTrace);
                return StatusCode(500, new { message = e.Message });
            }
        }

        /// <summary>
        /// Get bill info
        /// </summary>
        /// <param name="billId"></param>
        /// <param name="stylistId"></param>
        /// <returns></returns>
        [HttpGet("get-bill")]
        public async Task<IActionResult> GetBillInfo([FromQuery] int billId, int stylistId)
        {
            try
            {
                if (billId <= 0)
                {
                    return BadRequest(new { message = "BillId not exist" });
                }
                if (stylistId <= 0)
                {
                    return BadRequest(new { message = "StylistId not exist" });
                }
                // get
                var data = await BillService.GetBillInfo(billId, stylistId);
                //return
                return Ok(data);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", StackTrace: " + e.StackTrace);
                return StatusCode(500, new { message = e.Message });
            }
        }

        /// <summary>
        /// Get detail bill pending status
        /// </summary>
        /// <param name="billId">Id of bill</param>
        /// <returns></returns>
        [HttpGet("detail-billpending-status")]
        public async Task<IActionResult> GetDetailBillPendingStatus([FromQuery] int billId)
        {
            try
            {
                if (billId <= 0)
                {
                    return BadRequest(new { message = "BillId not exist!" });
                }

                var bill = await BillService.GetDetailBill(billId);
                if (bill != null)
                    return Ok(bill);
                else
                    return NotFound(new { message = "Bill not found!" });
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", StackTrace: " + e.StackTrace);
                return StatusCode(500, new { message = e.Message });
            }
        }

        /// <summary>
        /// Get all bill pending of salon today
        /// </summary>
        /// <param name="staffId">Id of Staff</param>
        /// will be remove in fewday
        /// <returns></returns>
        [HttpGet("billpending")]
        public async Task<IActionResult> GetAllBillPending([FromQuery] int staffId)
        {
            try
            {
                if (staffId <= 0)
                {
                    return BadRequest(new { message = "StaffId not exist!" });
                }

                var staff = await StaffRepo.Get(s => s.Id == staffId && s.IsDelete == 0 && s.Active == 1);
                if (staff == null)
                {
                    return NotFound(new { message = "Staff not found!" });
                }

                var list = await BillService.GetAllBillPending(staff);
                if (list == null)
                    list = new List<BillPendingItem> { };

                return Ok(list);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", StackTrace: " + e.StackTrace);
                return StatusCode(500, new { message = e.Message });
            }
        }

        //------------ Version linear ------------------------------------------------------------------------------

        /// <summary>
        /// Get all bill pending of a staff in salon today
        /// </summary>
        /// <param name="staffId">Id of Staff</param>
        /// <returns></returns>
        [HttpGet("billpending-v2")]
        public async Task<IActionResult> GetAllBillPendingOfStaff([FromQuery] int staffId)
        {
            try
            {
                if (staffId <= 0)
                {
                    return BadRequest(new { message = "StaffId not exist!" });
                }

                var staff = await StaffRepo.Get(s => s.Id == staffId && s.IsDelete == 0 && s.Active == 1);
                if (staff == null)
                {
                    return NotFound(new { message = "Staff not found!" });
                }

                var list = await BillService.GetAllBillPendingOfStaff(staff);
                if (list == null)
                    list = new List<BillPendingItem> { };

                return Ok(list);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", StackTrace: " + e.StackTrace);
                return StatusCode(500, new { message = e.Message });
            }
        }
    }
}