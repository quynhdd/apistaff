﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using APIStaff.Extentions;
using APIStaff.Models.Solution_30Shine;
using APIStaff.Repository.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using static APIStaff.Models.CustomeModel.InputModel;
using static APIStaff.Models.CustomeModel.OutputModel;

namespace APIStaff.Controllers
{
    [Produces("application/json")]
    [Route("api/stylemaster")]
    public class StyleMasterController : Controller
    {
        private ILogger<BillServiceController> Logger { get; }
        private readonly IPushNotice PushNotice;

        private readonly IStyleMasterRepo StyleMasterRepo;
        private readonly IStyleMasterLogRepo StyleMasterLogRepo;

        public StyleMasterController(ILogger<BillServiceController> logger, IPushNotice pushNotice,
            IStyleMasterRepo styleMasterRepo, IStyleMasterLogRepo styleMasterLogRepo)
        {
            Logger = logger;
            PushNotice = pushNotice;
            StyleMasterRepo = styleMasterRepo;
            StyleMasterLogRepo = styleMasterLogRepo;
        }

        /// <summary>
        /// Get the post with status
        /// </summary>
        /// <param name="staffId">Id of staff</param>
        /// <param name="type">1: Pending ; 2: NotReview ; 3: Reviewed</param>
        /// <returns></returns>
        [HttpGet("get-post-by-status")]
        public async Task<IActionResult> GetPostAsync([FromQuery] int staffId, [FromQuery] int type)
        {
            try
            {
                if (staffId <= 0)
                {
                    return BadRequest(new { message = "StaffId must greater than 0!" });
                }

                var list = await StyleMasterRepo.GetPostByStatus(staffId, type);
                return Ok(list);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", StackTrace: " + e.StackTrace);
                return StatusCode(500, new { message = e.Message });
            }
        }

        /// <summary>
        /// Get top StyleMaster Post in last 5 day
        /// </summary>
        /// <param name="userId">Id of staff or customer who get this posts</param>
        /// <param name="userType">0: Customer ; 1: Staff</param>
        /// <returns></returns>
        [HttpGet("new-post-five-day")]
        public async Task<IActionResult> GetTopFivePost([FromQuery] int userId, [FromQuery] int userType)
        {
            try
            {
                if (userId <= 0)
                {
                    return BadRequest(new { message = "UserId must greater than 0!" });
                }
                else if (userType != 0 && userType != 1)
                {
                    return BadRequest(new { message = "UserType must be 0 or 1!" });
                }

                TimeSpan timespan = new TimeSpan(23, 59, 59);
                TimeSpan timespan2 = new TimeSpan(01, 00, 01);
                DateTime ApproveTime = DateTime.Now;
                DateTime TimeFrom = ApproveTime.AddDays(-5);
                DateTime TimeTo = ApproveTime;
                if (TimeTo.Month == TimeFrom.Month)
                {
                    TimeFrom = ApproveTime.AddDays(-4).Add(timespan2);
                    TimeTo = ApproveTime.Add(timespan);
                }
                else
                {
                    TimeFrom = new DateTime(ApproveTime.Year, ApproveTime.Month, 1).Add(timespan2);
                    TimeTo = ApproveTime.Add(timespan);
                }

                List<StyleMasterPost> listRecord = null;
                if (userType == 0) // customer
                {
                    listRecord = await StyleMasterRepo.GetPostForCustomerByTimespan(userId, TimeFrom, TimeTo);
                }
                else // staff
                {
                    listRecord = await StyleMasterRepo.GetPostForStaffByTimespan(userId, TimeFrom, TimeTo);
                }
                return Ok(listRecord);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", StackTrace: " + e.StackTrace);
                return StatusCode(500, new { message = e.Message });
            }
        }

        /// <summary>
        /// For Staff, get list post in this month, sort by TotalLike desc
        /// For customer, get post in last 20 days
        /// </summary>
        /// <param name="userId">Id of staff or customer who get this posts</param>
        /// <param name="userType">0: Customer ; 1: Staff</param>
        /// <returns></returns>
        [HttpGet("top-like-in-month")]
        public async Task<IActionResult> GetTopPostInMonth([FromQuery] int userId, [FromQuery] int userType, [FromQuery] int pageIndex, [FromQuery] int pageSize)
        {
            try
            {
                if (userId <= 0)
                {
                    return BadRequest(new { message = "UserId must greater than 0!" });
                }
                else if (userType != 0 && userType != 1)
                {
                    return BadRequest(new { message = "UserType must be 0 or 1!" });
                }
                else if (pageIndex <= 0 || pageSize <= 0)
                {
                    return BadRequest(new { message = "PageSize and PageIndex must greater than 0!" });
                }

                List<StyleMasterPost> listRecord = null;
                if (userType == 0) // customer
                {
                    var today = DateTime.Today;
                    var tomorrow = today.AddDays(1);
                    var twentyDayAgo = today.AddDays(-20);
                    listRecord = await StyleMasterRepo.GetPostForCustomerPagging(userId, twentyDayAgo, tomorrow, pageIndex, pageSize);
                }
                else // staff
                {
                    var firstDayOfMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                    var lastDayOfMonth = firstDayOfMonth.AddMonths(1);
                    listRecord = await StyleMasterRepo.GetPostForStaffPagging(userId, firstDayOfMonth, lastDayOfMonth, pageIndex, pageSize);
                }
                return Ok(listRecord);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", StackTrace: " + e.StackTrace);
                return StatusCode(500, new { message = e.Message });
            }
        }

        /// <summary>
        /// Get top 3 post in last 3 month
        /// </summary>
        /// <returns></returns>
        [HttpGet("top-like-three-month")]
        public async Task<IActionResult> GetTopPostInMonth()
        {
            try
            {
                List<TopStyleMasterResponse> topLikeList = new List<TopStyleMasterResponse>();

                for (int i = 1; i <= 3; i++)
                {
                    DateTime ApproveTime = DateTime.Now.AddMonths(-i);
                    int Month = ApproveTime.Month;
                    int Year = ApproveTime.Year;
                    DateTime timeFrom = new DateTime(Year, Month, 1);
                    DateTime timeTo = timeFrom.AddMonths(1);

                    TopStyleMasterResponse record = new TopStyleMasterResponse();
                    record.PostOfMonth = Month + "-" + Year;
                    record.Origin = await StyleMasterRepo.GetTopPostByTimespan(timeFrom, timeTo, 3);

                    topLikeList.Add(record);
                }

                return Ok(topLikeList);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", StackTrace: " + e.StackTrace);
                return StatusCode(500, new { message = e.Message });
            }
        }

        /// <summary>
        /// Get top post that user like nearest
        /// </summary>
        /// <param name="userId">Id of staff or customer</param>
        /// <param name="userType">0: Customer ; 1: Staff</param>
        /// <returns></returns>
        [HttpGet("top-nearly-like-post")]
        public async Task<IActionResult> GetTopNearlyLikePost([FromQuery] int userId, [FromQuery] int userType, [FromQuery] int top)
        {
            try
            {
                if (userId <= 0)
                {
                    return BadRequest(new { message = "UserId must greater than 0!" });
                }
                else if (userType != 0 && userType != 1)
                {
                    return BadRequest(new { message = "UserType must be 0 or 1!" });
                }
                else if (top <= 0)
                {
                    return BadRequest(new { message = "Top must greater than 0!" });
                }

                List<StyleMasterPost> listRecord = new List<StyleMasterPost>();
                if (userType == 0) // customer
                {
                    listRecord = await StyleMasterRepo.GetNearlyLikePost(userId, top);
                }
                return Ok(listRecord);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", StackTrace: " + e.StackTrace);
                return StatusCode(500, new { message = e.Message });
            }
        }

        /// <summary>
        /// Update vote StyleMaster for Customer and Staff
        /// </summary>
        /// Sample request:
        ///     {
        ///        "PostNumber": '0003', // Number code of StyleMaster Post
        ///        "Like": 1,           // 0: unlike ; 1: like
        ///        "UserId": 124,      // Id of staff or customer who like this post
        ///        "UserType": 1      // 0: Customer like ; 1: Staff like
        ///     }
        /// <returns></returns>
        [HttpPut("update-vote")]
        public async Task<IActionResult> UpdateStyleMasterVote([FromBody] ReqUpdateVoteStyleMaster data)
        {
            try
            {
                if (data.UserId <= 0)
                {
                    return BadRequest(new { message = "UserId must greater than 0!" });
                }
                else if (data.UserType != 0 && data.UserType != 1)
                {
                    return BadRequest(new { message = "UserType must be 0 or 1!" });
                }
                else if (data.Like != 0 && data.Like != 1)
                {
                    return BadRequest(new { message = "Like must be 0 or 1!" });
                }
                else if (string.IsNullOrEmpty(data.PostNumber))
                {
                    return BadRequest(new { message = "PostNumber must not be empty!" });
                }

                var styleMasterPost = await StyleMasterRepo.GetPostByPostNumber(data.PostNumber);
                if (styleMasterPost == null)
                {
                    return BadRequest(new { message = "The post not found!" });
                }
                var logMaster = new StyleMasterLog();
                switch (data.Like)
                {
                    case 1:
                        styleMasterPost.TotalLike += 1;
                        StyleMasterRepo.Update(styleMasterPost);

                        logMaster.PostNumber = styleMasterPost.PostNumber;
                        logMaster.StyleMasterId = styleMasterPost.Id;
                        logMaster.PostedDate = styleMasterPost.CreatedTime;
                        if (data.UserType == 0) // customer
                        {
                            logMaster.CustomerId = data.UserId;
                            logMaster.StaffId = 0;
                        }
                        else // staff
                        {
                            logMaster.CustomerId = 0;
                            logMaster.StaffId = data.UserId;
                        }
                        StyleMasterLogRepo.Add(logMaster);

                        await StyleMasterLogRepo.SaveChanges();
                        break;
                    case 0:
                        styleMasterPost.TotalLike -= 1;
                        StyleMasterRepo.Update(styleMasterPost);

                        if (data.UserType == 0) // customer
                        {
                            logMaster = await StyleMasterLogRepo.Get(s => s.PostNumber == data.PostNumber && s.CustomerId == data.UserId && s.StyleMasterId == styleMasterPost.Id);
                        }
                        else // staff
                        {
                            logMaster = await StyleMasterLogRepo.Get(s => s.PostNumber == data.PostNumber && s.StaffId == data.UserId && s.StyleMasterId == styleMasterPost.Id);
                        }
                        StyleMasterLogRepo.Remove(logMaster);

                        await StyleMasterLogRepo.SaveChanges();
                        break;
                }

                return Ok();
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", StackTrace: " + e.StackTrace);
                return StatusCode(500, new { message = e.Message });
            }
        }
    }
}