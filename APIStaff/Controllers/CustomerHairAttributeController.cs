﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APIStaff.Extentions;
using APIStaff.Models.CustomeModel;
using APIStaff.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace APIStaff.Controllers
{
    [Produces("application/json")]
    [Route("api/customer-hair")]
    public class CustomerHairAttributeController : Controller
    {
        private ILogger<CustomerHairAttributeController> Logger { get; }
        private readonly IPushNotice PushNotice;
        private readonly Customer Customer;

        //constructor
        public CustomerHairAttributeController(ILogger<CustomerHairAttributeController> logger,
                                     IPushNotice pushNotice,
                                     Customer customer)
        {
            Logger = logger;
            PushNotice = pushNotice;
            Customer = customer;
        }

        /// <summary>
        /// AddOrUpdate FlowProduct
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> AddOrUpdate([FromBody] InputModel.ReqCustomerHairAttribute param)
        {
            try
            {
                if (param == null || param.CustomerId <= 0 || param.BillId <= 0 || param.UserId <= 0)
                {
                    return BadRequest(new { Message = "Data does not exist!" });
                }
                await Customer.AddOrUpdate(param.UserId, param.CustomerId, param.BillId, param.Note);
                return Ok(new { Message = "Success!" });
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", Inner: " + e.StackTrace);
                return StatusCode(500, new { message = e.Message });
            }
        }
    }
}