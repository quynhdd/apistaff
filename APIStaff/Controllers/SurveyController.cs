﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using APIStaff.Models.Dynamo;
using APIStaff.Repository.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace APIStaff.Controllers
{
    [Produces("application/json")]
    [Route("api/survey")]
    public class SurveyController : Controller
    {
        private IDynamoDbContext<SurveyQuestion> SurveyQuestionContext;
        private IDynamoDbContext<SurveyAnswer> SurveyAnswerContext;
        private IDynamoDbContext<SurveyUser> SurveyUserContext;
        private IDynamoDbContext<SurveyQuestionType> SurveyQuestionTypeContext;
        private IStaffRepo StaffRepo;
        private readonly IConfiguration Configuration;
        static HttpClient client = new HttpClient();

        public SurveyController(IDynamoDbContext<SurveyQuestion> surveyQuestionContext, IDynamoDbContext<SurveyAnswer> surveyAnswerContext, IDynamoDbContext<SurveyUser> surveyUserContext, IDynamoDbContext<SurveyQuestionType> surveyQuestionTypeContext, IStaffRepo staffRepo, IConfiguration configuration)
        {
            SurveyQuestionContext = surveyQuestionContext;
            SurveyAnswerContext = surveyAnswerContext;
            SurveyUserContext = surveyUserContext;
            SurveyQuestionTypeContext = surveyQuestionTypeContext;
            StaffRepo = staffRepo;
            Configuration = configuration;
        }

        [HttpGet]
        public async Task<IActionResult> GetSurvey([FromQuery] int staffId)
        {
            try
            {
                if (staffId <= 0)
                {
                    return BadRequest();
                }

                var staff = await StaffRepo.Get(a => a.Id == staffId && a.IsDelete == 0 && a.Active == 1);

                if (staff == null)
                {
                    return NoContent();
                }

                List<ScanCondition> scanConditions = new List<ScanCondition>();
                scanConditions.Add(new ScanCondition("UserId", ScanOperator.Equal, staffId));
                scanConditions.Add(new ScanCondition("UserType", ScanOperator.Equal, 1));

                var surveyUsers = await SurveyUserContext.GetByAttribute(scanConditions);

                if (!surveyUsers.Any())
                {
                    return NoContent();
                }

                var survey = surveyUsers.FirstOrDefault();

                if (survey.IsAnswer == true)
                {
                    return NoContent();
                }

                var questionId = survey.QuestionId;

                var userQuestion = await SurveyQuestionContext.GetByIdAsync(questionId);

                if (userQuestion == null)
                {
                    return NoContent();
                }

                if (userQuestion.EndDate < DateTime.Today)
                {
                    await SurveyUserContext.DeleteByIdAsync(surveyUsers.FirstOrDefault());
                }

                if (userQuestion.Type == 3)
                {
                    List<ScanCondition> answerCondition = new List<ScanCondition>();
                    answerCondition.Add(new ScanCondition("QuestionId", ScanOperator.Equal, questionId));

                    var answers = await SurveyAnswerContext.GetByAttribute(answerCondition);

                    if (!answers.Any())
                    {
                        return NoContent();
                    }

                    var data = new
                    {
                        userQuestion.Id,
                        userQuestion.Question,
                        userQuestion.Type,
                        userQuestion.Image,
                        answers = answers.Select(a => new { a.Id, a.Answer })
                    };

                    return Ok(data);
                }


                var returnData = new
                {
                    userQuestion.Id,
                    userQuestion.Question,
                    userQuestion.Type,
                    userQuestion.Image,
                    answers = new List<object>()
                };

                return Ok(returnData);

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpPost]
        public async Task<IActionResult> AnswerSurvey([FromBody] SurveyAnswerJson data)
        {
            try
            {
                if (data.StaffId <= 0)
                {
                    return BadRequest();
                }

                var staff = await StaffRepo.Get(a => a.Id == data.StaffId && a.IsDelete == 0 && a.Active == 1);

                if (staff == null)
                {
                    return NoContent();
                }

                List<ScanCondition> scanConditions = new List<ScanCondition>();
                scanConditions.Add(new ScanCondition("UserId", ScanOperator.Equal, data.StaffId));
                scanConditions.Add(new ScanCondition("UserType", ScanOperator.Equal, 1));
                scanConditions.Add(new ScanCondition("QuestionId", ScanOperator.Equal, data.QuestionId));

                var surveyUsers = await SurveyUserContext.GetByAttribute(scanConditions);

                if (!surveyUsers.Any())
                {
                    return NoContent();
                }

                var userQuestion = await SurveyQuestionContext.GetByIdAsync(data.QuestionId);

                if (userQuestion == null)
                {
                    return NoContent();
                }

                if (userQuestion.EndDate < DateTime.Today)
                {
                    await SurveyUserContext.DeleteByIdAsync(surveyUsers.FirstOrDefault());
                }

                if (userQuestion.Type == 3 && string.IsNullOrEmpty(data.AnswerId))
                {
                    return BadRequest();
                }

                string answer = string.Empty;

                if (userQuestion.Type == 3)
                {
                    List<ScanCondition> answerCondition = new List<ScanCondition>();
                    answerCondition.Add(new ScanCondition("Id", ScanOperator.In, data.AnswerId.Split(",").ToList().ToArray()));

                    var answerContent = await SurveyAnswerContext.GetByAttribute(answerCondition);

                    if (!answerContent.Any())
                    {
                        return NoContent();
                    }

                    answer = string.Join(",", answerContent.Select(a => a.Answer).ToList());
                }
                else
                {
                    answer = data.Answer;
                }

                var dataPost = new
                {
                    id = staff.Id,
                    phone = staff.Phone,
                    name = staff.Fullname,
                    type = "Staff",
                    questionId = data.QuestionId,
                    question = userQuestion.Question,
                    answer = answer,
                    answerTime = DateTime.Now.ToString("HH:mm dd/MM/yyyy")
                };

                var domainGooglePostAPI = Configuration.GetSection("GoogleSpreadSheetAPI:Survey").Value;

                var bookingContent = JsonConvert.SerializeObject(dataPost);
                var buffer = System.Text.Encoding.UTF8.GetBytes(bookingContent);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                var res = await client.PostAsync(domainGooglePostAPI, byteContent);

                var survey = surveyUsers.FirstOrDefault();

                survey.IsAnswer = true;

                await SurveyUserContext.SaveAsync(survey);

                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        public class SurveyAnswerJson
        {
            public int StaffId { get; set; }
            public string QuestionId { get; set; }
            public string Answer { get; set; }
            public string AnswerId { get; set; }

        }
    }
}