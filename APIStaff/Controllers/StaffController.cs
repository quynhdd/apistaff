﻿using System;
using System.Collections.Generic;
using System.Linq;
using APIStaff.Models.CustomeModel;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using APIStaff.Repository.Interface;
using Microsoft.Extensions.Logging;
using APIStaff.Extentions;
using System.Globalization;
using static APIStaff.Models.CustomeModel.OutputModel;
using APIStaff.Models.Solution_30Shine;


namespace APIStaff.Controllers
{
    [Produces("application/json")]
    [Route("api/staff")]
    public class StaffController : Controller
    {
        private readonly CultureInfo culture = new CultureInfo("vi-VN");
        private readonly IStaffRepo StaffRepo;
        private readonly ILogger Logger;
        private readonly IPushNotice PushNotice;
        private readonly IFlowTimeKeepingRepo FlowTimeKeepingRepo;
        public StaffController(IStaffRepo staffRepo, ILogger<StaffController> logger, IPushNotice pushNotice, IFlowTimeKeepingRepo flowTimeKeepingRepo)
        {
            this.StaffRepo = staffRepo;
            this.FlowTimeKeepingRepo = flowTimeKeepingRepo;
            this.Logger = logger;
            this.PushNotice = pushNotice;
        }

        [HttpGet("bills")]
        public async Task<IActionResult> GetRecentStylistBills([FromQuery] int stylistId, string from, string to)
        {
            try
            {
                DateTime dateFrom = Convert.ToDateTime(from, culture).Date;
                DateTime dateTo = Convert.ToDateTime(to, culture).AddDays(1).Date;
                List<StylistBill> data = await StaffRepo.GetStylistBills(stylistId, dateFrom, dateTo);
                if (data == null) return NotFound("Data not found.");
                return Ok(data);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", StackTrace: " + e.StackTrace);
                return StatusCode(500, new { message = e.Message });
            }
        }

        [HttpGet("staff-members")]
        public async Task<IActionResult> GetStaffMembers([FromQuery] int staffId)
        {
            try
            {
                if (staffId <= 0)
                {
                    return BadRequest("staffId must be greater than 0!");
                }
                DateTime workDate = DateTime.Today;
                FlowTimeKeeping flowTimeKeeping = await FlowTimeKeepingRepo.Get(s => s.StaffId == staffId && s.WorkDate == workDate && s.IsEnroll == true);
                if (flowTimeKeeping is null)
                {
                    return NotFound("Flow time keeping not found");
                }
                List<StaffMember> staffMembers = await StaffRepo.getStaffMembersBySalonId(flowTimeKeeping.SalonId, workDate);
                if (staffMembers.Count == 0) return NotFound("Staff members not found.");

                return Ok(staffMembers);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
                PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", StackTrace: " + e.StackTrace);
                return StatusCode(500, new { message = e.Message });
            }
        }

		[HttpGet("cutscore")]
		public async Task<IActionResult> GetStylistCutQualityScore([FromQuery] int stylistId, string from, string to)
		{
			try
			{
				DateTime dateFrom = Convert.ToDateTime(from, culture).Date;
				DateTime dateTo = Convert.ToDateTime(to, culture).AddDays(1).Date;
				object data = await StaffRepo.GetStylistCutQualityScore(stylistId, dateFrom, dateTo);
				if (data == null) return NotFound("Data not found.");
				return Ok(data);
			}
			catch (Exception e)
			{
				Logger.LogError(e.Message);
				PushNotice.PushErrorToSlack(this.GetType().Name, System.Reflection.MethodBase.GetCurrentMethod().Name, e.Message + ", StackTrace: " + e.StackTrace);
				return StatusCode(500, new { message = e.Message });
			}
		}


	}
}