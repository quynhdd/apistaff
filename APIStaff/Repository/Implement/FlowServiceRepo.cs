﻿using read=APIStaff.Models.Solution_30Shine_BI;
using APIStaff.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static APIStaff.Models.CustomeModel.OutputModel;

namespace APIStaff.Repository.Implement
{
    public class FlowServiceRepo : IFlowServiceRepo
    {
        private readonly read.Solution_30shineContext dbContext;
        private readonly DbSet<read.FlowService> dbSet;
        //constructor
        public FlowServiceRepo(read.Solution_30shineContext dbContext)
        {
            this.dbContext = dbContext;
            dbSet = dbContext.Set<read.FlowService>();
        }

        /// <summary>
        ///  Get service data
        /// </summary>
        /// <param name="billId"></param>
        /// <returns></returns>
        public async Task<List<ServiceData>> GetServiceData(int billId)
        {
            try
            {
                return await (from a in dbContext.FlowService
                              join b in dbContext.Service on a.ServiceId equals b.Id
                              where
                              a.BillId == billId &&
                              a.IsDelete == 0
                              select new ServiceData
                              {
                                  ServiceId = b.Id,
                                  Name = b.Name,
                                  Quantity = a.Quantity ?? 0
                              }).ToListAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Count service Data
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <returns></returns>
        public async Task<List<ServiceData>> CountServiceData(int CustomerId)
        {
            try
            {
                int Dmn = 90, Tdc = 91;
                var sql = $@"SELECT COUNT(ISNULL(a.Quantity, 0)) AS Quantity, a.ServiceId FROM dbo.FlowService a
                                             INNER JOIN dbo.BillService b ON a.BillId = b.Id
                                             WHERE a.IsDelete = 0 AND b.Pending = 0 AND 
                                             b.IsDelete = 0 AND b.CustomerId = {CustomerId} 
                                             AND a.ServiceId IN ({Dmn}, {Tdc})
                                             GROUP BY a.ServiceId";
                var list = new List<ServiceData>();
                var command = dbContext.Database.GetDbConnection().CreateCommand();
                command.CommandText = sql;
                dbContext.Database.OpenConnection();
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    var item = new ServiceData();
                    item.ServiceId = Convert.ToInt32(reader["ServiceId"]);
                    item.Quantity = Convert.ToInt32(reader["Quantity"]);
                    list.Add(item);
                }
                reader.Close();
                reader.Dispose();
                command.Dispose();
                return list;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
