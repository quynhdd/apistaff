﻿using APIStaff.Models.Solution_30Shine;
using APIStaff.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIStaff.Repository.Implement
{
    public class WorkTimeRepo : IWorkTimeRepo
    {
        private readonly Solution_30shineContext dbContext;
        private readonly DbSet<WorkTime> dbSet;
        //constructor
        public WorkTimeRepo(Solution_30shineContext dbContext)
        {
            this.dbContext = dbContext;
            dbSet = dbContext.Set<WorkTime>();
        }

        public async Task<WorkTime> GetById(int workTimeId)
        {
            try
            {
                return await dbSet.FirstOrDefaultAsync(w => w.IsDelete == 0 && w.Publish == true && w.Id == workTimeId);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
