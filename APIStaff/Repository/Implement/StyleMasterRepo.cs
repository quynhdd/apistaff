﻿using APIStaff.Models.Solution_30Shine;
using APIStaff.Repository.Interface;
using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using static APIStaff.Models.CustomeModel.OutputModel;
using System.Linq;
using APIStaff.Extentions;

namespace APIStaff.Repository.Implement
{
    public class StyleMasterRepo : IStyleMasterRepo
    {
        private readonly Solution_30shineContext dbContext;
        private readonly DbSet<StyleMaster> dbSet;

        public StyleMasterRepo(Solution_30shineContext dbContext)
        {
            this.dbContext = dbContext;
            dbSet = dbContext.Set<StyleMaster>();
        }

        public async Task<List<MyStyleMasterPost>> GetPostByStatus(int staffId, int status)
        {
            try
            {
                var list = await (from a in dbContext.StyleMaster
                                  where a.StylistId == staffId && a.StyleMasterStatusId == status && a.IsDelete == false
                                  orderby a.PostNumber descending
                                  select new MyStyleMasterPost
                                  {
                                      PostNumber = a.PostNumber,
                                      TotalLikes = a.TotalLike,
                                      Img_1 = a.Image1,
                                      Img_2 = a.Image2,
                                      Img_3 = a.Image3,
                                      Img_4 = a.Image4,
                                      Date = String.Format("{0:dd/MM/yyyy}", a.CreatedTime)
                                  }
                            ).ToListAsync();
                return list;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<List<StyleMasterPost>> GetPostForCustomerByTimespan(int customerId, DateTime timeFrom, DateTime timeTo)
        {
            try
            {
                var list = await (from a in dbContext.StyleMaster
                                  from b in dbContext.StyleMasterLog.Where(r => r.PostNumber == a.PostNumber && r.StyleMasterId == a.Id && r.CustomerId == customerId).DefaultIfEmpty()
                                  where a.ApproveTime >= timeFrom && a.ApproveTime < timeTo && a.IsDelete == false
                                  && a.StyleMasterStatusId == AppConstants.STYLE_MASTER_APPROVE_STATUS
                                  orderby a.ApproveTime descending, a.TotalLike descending, a.Id descending
                                  select new StyleMasterPost
                                  {
                                      PostNumber = a.PostNumber,
                                      StylistName = a.StylistName,
                                      TotalLikes = a.TotalLike,
                                      Img_1 = a.Image1,
                                      Img_2 = a.Image2,
                                      Img_3 = a.Image3,
                                      Img_4 = a.Image4,
                                      IsLiked = b.CustomerId == null ? false : true,
                                      UseCurling = a.UseCurling ?? false,
                                      UseHairDye = a.UseHairDye ?? false
                                  }
                            ).ToListAsync();
                return list;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<List<StyleMasterPost>> GetPostForStaffByTimespan(int staffId, DateTime timeFrom, DateTime timeTo)
        {
            try
            {
                var list = await (from a in dbContext.StyleMaster
                                  from b in dbContext.StyleMasterLog.Where(r => r.PostNumber == a.PostNumber && r.StyleMasterId == a.Id && r.StaffId == staffId).DefaultIfEmpty()
                                  where a.ApproveTime >= timeFrom && a.ApproveTime < timeTo && a.IsDelete == false
                                  && a.StyleMasterStatusId == AppConstants.STYLE_MASTER_APPROVE_STATUS
                                  orderby a.ApproveTime descending, a.TotalLike descending, a.Id descending
                                  select new StyleMasterPost
                                  {
                                      PostNumber = a.PostNumber,
                                      StylistName = a.StylistName,
                                      TotalLikes = a.TotalLike,
                                      Img_1 = a.Image1,
                                      Img_2 = a.Image2,
                                      Img_3 = a.Image3,
                                      Img_4 = a.Image4,
                                      IsLiked = b.StaffId == null ? false : true,
                                      UseCurling = a.UseCurling ?? false,
                                      UseHairDye = a.UseHairDye ?? false
                                  }
                            ).ToListAsync();
                return list;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<List<StyleMasterPost>> GetPostForCustomerPagging(int customerId, DateTime timeFrom, DateTime timeTo, int pageIndex, int pageSize)
        {
            try
            {
                var list = await (from a in dbContext.StyleMaster
                                  from b in dbContext.StyleMasterLog.Where(r => r.PostNumber == a.PostNumber && r.StyleMasterId == a.Id && r.CustomerId == customerId).DefaultIfEmpty()
                                  where a.ApproveTime >= timeFrom && a.ApproveTime < timeTo && a.IsDelete == false
                                  && a.StyleMasterStatusId == AppConstants.STYLE_MASTER_APPROVE_STATUS
                                  orderby a.TotalLike descending, a.ApproveTime descending, a.Id descending
                                  select new StyleMasterPost
                                  {
                                      PostNumber = a.PostNumber,
                                      StylistName = a.StylistName,
                                      TotalLikes = a.TotalLike,
                                      Img_1 = a.Image1,
                                      Img_2 = a.Image2,
                                      Img_3 = a.Image3,
                                      Img_4 = a.Image4,
                                      IsLiked = b.CustomerId == null ? false : true,
                                      UseCurling = a.UseCurling ?? false,
                                      UseHairDye = a.UseHairDye ?? false
                                  }
                           ).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();
                return list;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<List<StyleMasterPost>> GetPostForStaffPagging(int staffId, DateTime timeFrom, DateTime timeTo, int pageIndex, int pageSize)
        {
            try
            {
                var list = await (from a in dbContext.StyleMaster
                                  from b in dbContext.StyleMasterLog.Where(r => r.PostNumber == a.PostNumber && r.StyleMasterId == a.Id && r.StaffId == staffId).DefaultIfEmpty()
                                  where a.ApproveTime >= timeFrom && a.ApproveTime < timeTo && a.IsDelete == false
                                  && a.StyleMasterStatusId == AppConstants.STYLE_MASTER_APPROVE_STATUS
                                  orderby a.TotalLike descending, a.ApproveTime descending, a.Id descending
                                  select new StyleMasterPost
                                  {
                                      PostNumber = a.PostNumber,
                                      StylistName = a.StylistName,
                                      TotalLikes = a.TotalLike,
                                      Img_1 = a.Image1,
                                      Img_2 = a.Image2,
                                      Img_3 = a.Image3,
                                      Img_4 = a.Image4,
                                      IsLiked = b.StaffId == null ? false : true,
                                      UseCurling = a.UseCurling ?? false,
                                      UseHairDye = a.UseHairDye ?? false
                                  }
                           ).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();
                return list;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<List<StyleMasterPost>> GetTopPostByTimespan(DateTime timeFrom, DateTime timeTo, int top)
        {
            try
            {
                var list = await (from a in dbContext.StyleMaster
                                  where a.ApproveTime >= timeFrom && a.ApproveTime < timeTo && a.IsDelete == false
                                  && a.StyleMasterStatusId == AppConstants.STYLE_MASTER_APPROVE_STATUS
                                  orderby a.TotalLike descending, a.ApproveTime descending, a.Id descending
                                  select new StyleMasterPost
                                  {
                                      PostNumber = a.PostNumber,
                                      StylistName = a.StylistName,
                                      TotalLikes = a.TotalLike,
                                      Img_1 = a.Image1,
                                      Img_2 = a.Image2,
                                      Img_3 = a.Image3,
                                      Img_4 = a.Image4,
                                      UseCurling = a.UseCurling ?? false,
                                      UseHairDye = a.UseHairDye ?? false
                                  }
                            ).Take(top).ToListAsync();

                return list;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<List<StyleMasterPost>> GetNearlyLikePost(int customerId, int top)
        {
            try
            {
                var list = await (from a in dbContext.StyleMasterLog
                                  from b in dbContext.StyleMaster.Where(r => r.PostNumber == a.PostNumber && r.Id == a.StyleMasterId).DefaultIfEmpty()
                                  where b.IsDelete == false && b.StyleMasterStatusId == AppConstants.STYLE_MASTER_APPROVE_STATUS && a.CustomerId == customerId
                                  orderby a.Id descending
                                  select new StyleMasterPost
                                  {
                                      PostNumber = a.PostNumber,
                                      StylistName = b.StylistName,
                                      TotalLikes = b.TotalLike,
                                      Img_1 = b.Image1,
                                      Img_2 = b.Image2,
                                      Img_3 = b.Image3,
                                      Img_4 = b.Image4,
                                      IsLiked = true,
                                      UseCurling = b.UseCurling ?? false,
                                      UseHairDye = b.UseHairDye ?? false
                                  }
                           ).Take(top).ToListAsync();
                return list;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<StyleMaster> GetPostByPostNumber(string PostNumber)
        {
            try
            {
                return await dbSet.FirstOrDefaultAsync(s => s.PostNumber == PostNumber && s.IsDelete == false);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void Update(StyleMaster styleMaster)
        {
            try
            {
                dbContext.Update(styleMaster);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task SaveChanges()
        {
            try
            {
                await dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
