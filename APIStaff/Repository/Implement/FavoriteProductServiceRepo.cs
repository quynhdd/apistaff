﻿using APIStaff.Models.Solution_30Shine;
using APIStaff.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static APIStaff.Models.CustomeModel.OutputModel;

namespace APIStaff.Repository.Implement
{
    public class FavoriteProductServiceRepo : IFavoriteProductServiceRepo
    {
        private readonly Solution_30shineContext dbContext;
        private readonly DbSet<StaffFavoriteProductService> dbSet;

        public FavoriteProductServiceRepo(Solution_30shineContext dbContext)
        {
            this.dbContext = dbContext;
            dbSet = dbContext.Set<StaffFavoriteProductService>();
        }

        /// <summary>
        /// Get all favorite product of a staff
        /// Type: Product : 1 ; Service : 2
        /// </summary>
        public async Task<List<ProductModel>> GetFavoriteProduct(int staffId)
        {
            try
            {
                var list = await (from favorite in dbContext.StaffFavoriteProductService
                                  join product in dbContext.Product on favorite.ProductServiceId equals product.Id into ResultGroup
                                  from result in ResultGroup.DefaultIfEmpty()
                                  where favorite.StaffId == staffId && favorite.Type == 1
                                  select new ProductModel
                                  {
                                      Id = result.Id,
                                      Name = result.Name
                                  }).ToListAsync();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get all favorite product of a staff
        /// Type: Product : 1 ; Service : 2
        /// </summary>
        public async Task<List<ServiceModel>> GetFavoriteService(int staffId)
        {
            try
            {
                var list = await (from favorite in dbContext.StaffFavoriteProductService
                                  join service in dbContext.Service on favorite.ProductServiceId equals service.Id into ResultGroup
                                  from result in ResultGroup.DefaultIfEmpty()
                                  where favorite.StaffId == staffId && favorite.Type == 2
                                  select new ServiceModel
                                  {
                                      Id = result.Id,
                                      Name = result.Name
                                  }).ToListAsync();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
