﻿using read = APIStaff.Models.Solution_30Shine_BI;
using write = APIStaff.Models.Solution_30Shine;
using APIStaff.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIStaff.Repository.Implement
{
    public class CustomerHairAttributeRepo : ICustomerHairAttributeRepo
    {
        //only read
        private readonly read.Solution_30shineContext dbContextRead;
        private readonly DbSet<read.CustomerHairAttribute> dbSetRead;

        //insert,update
        private readonly write.Solution_30shineContext dbContextWrite;
        private readonly DbSet<write.CustomerHairAttribute> dbSetWrite;

        public CustomerHairAttributeRepo(read.Solution_30shineContext dbContextRead, write.Solution_30shineContext dbContextWrite)
        {
            this.dbContextRead = dbContextRead;
            dbSetRead = dbContextRead.Set<read.CustomerHairAttribute>();
            this.dbContextWrite = dbContextWrite;
            dbSetWrite = dbContextWrite.Set<write.CustomerHairAttribute>();
        }

        public async Task<write.CustomerHairAttribute> GetByBillId(int BillId)
        {
            return await dbSetWrite.FirstOrDefaultAsync(w => w.BillServiceId == BillId);
        }

        /// <summary>
        /// Update record of Table StaticServiceProfit
        /// </summary>
        /// <param name="objProfit"></param>
        public void Update(write.CustomerHairAttribute record)
        {
            try
            {
                dbSetWrite.Update(record);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Add record of table Flowproduct
        /// </summary>
        /// <param name="objFlowProduct"></param>
        public void Add(write.CustomerHairAttribute record)
        {
            try
            {
                dbSetWrite.Add(record);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Save change Async
        /// </summary>
        /// <returns></returns>
        public async Task SaveChangeAsync()
        {
            try
            {
                await dbContextWrite.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
