﻿using read = APIStaff.Models.Solution_30Shine_BI;
using write = APIStaff.Models.Solution_30Shine;
using APIStaff.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using static APIStaff.Models.CustomeModel.OutputModel;
using Newtonsoft.Json;

namespace APIStaff.Repository.Implement
{
    public class BillServiceRepo : IBillServiceRepo
    {
        // only read statistic
        private readonly read.Solution_30shineContext dbContextRead;
        private readonly DbSet<read.BillService> dbSetRead;

        // insert or update and read
        private readonly write.Solution_30shineContext dbContextWrite;
        private readonly DbSet<write.BillService> dbSetWrite;

        public BillServiceRepo(read.Solution_30shineContext dbContextRead, write.Solution_30shineContext dbContextWrite)
        {
            this.dbContextRead = dbContextRead;
            dbSetRead = dbContextRead.Set<read.BillService>();
            this.dbContextWrite = dbContextWrite;
            dbSetWrite = dbContextWrite.Set<write.BillService>();
        }

        /*
         * Chỉ có duy nhất 2 API dùng đó là 1 API của Dũng( đã bỏ ko dùng nữa) và 1 API  của a Quỳnh (có vẻ cũng ko dùng nữa)
         * Tất cả những funtion khác đều phục vụ cho BillPending nên phải dùng dbWrite
         */
        public async Task<read.BillService> GetDbRead(Expression<Func<read.BillService, bool>> expression)
        {
            try
            {
                return await dbSetRead.FirstOrDefaultAsync(expression);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<write.BillService> GetDbWrite(Expression<Func<write.BillService, bool>> expression)
        {
            try
            {
                return await dbSetWrite.FirstOrDefaultAsync(expression);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Tuple<double, bool>> GetWaitTimeSinceLastService(int skinnerId)
        {
            var tmp = await (
                        from b in dbContextWrite.BillService
                        where b.StaffHairMassageId == skinnerId && b.CreatedDate > DateTime.Now.Date
                            && (b.IsDelete == null || b.IsDelete != 1) && b.Pending == 1
                        orderby b.CreatedDate descending
                        select Tuple.Create(getWaitTime(b.InProcedureTime), b.Id != 0)
                      ).FirstOrDefaultAsync();
            return tmp;
        }

        private double getWaitTime(DateTime? time)
        {
            Console.WriteLine("in procedure time " + time);
            if (time == null) return 0;
            TimeSpan? tmp = DateTime.Now - time;
            TimeSpan timeSpan;
            if (tmp == null) return 0;
            else timeSpan = (TimeSpan)tmp;

            return timeSpan.TotalMinutes;
        }

        public async Task<bool> Update(write.BillService bill)
        {
            try
            {
                dbContextWrite.BillService.Update(bill);
                if (!(dbContextWrite.SaveChanges() > 0))
                {
                    throw new Exception("Có lỗi xảy ra. Vui lòng liện hệ nhóm phát triển.");
                }
                else return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DetailBillPendingStatus> GetDetailBillPendingStatus(int billId)
        {
            try
            {
                var billPending = await (from bill in dbContextWrite.BillService
                                         join stylist in dbContextWrite.Staff on bill.StaffHairdresserId equals stylist.Id into FirstGroup
                                         from first in FirstGroup.DefaultIfEmpty()
                                         join skinner in dbContextWrite.Staff on bill.StaffHairMassageId equals skinner.Id into SecondGroup
                                         from second in SecondGroup.DefaultIfEmpty()
                                         where bill.Id == billId
                                         select new DetailBillPendingStatus
                                         {
                                             BillId = bill.Id,
                                             StylistId = first.Id,
                                             StylistName = first.Fullname,
                                             StylistCode = first.OrderCode,
                                             SkinnerId = second.Id,
                                             SkinnerName = second.Fullname,
                                             SkinnerCode = second.OrderCode,
                                             EstimateTimeCut = bill.EstimateTimeCut ?? 0,
                                             IsSulphite = bill.IsSulphite ?? false
                                         }).FirstOrDefaultAsync();
                return billPending;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get all bill pending of salon today
        /// </summary>
        /// <param name="salonId">0: Get Bill of all salon ; >0 : Get bill of this salon</param>
        public async Task<List<BillPendingItem>> GetAllBillPending(int staffId, int salonId, DateTime timeFrom)
        {
            try
            {
                var list = await (from bill in dbContextWrite.BillService
                                  join customer in dbContextWrite.Customer on bill.CustomerId equals customer.Id
                                  join salon in dbContextWrite.TblSalon on bill.SalonId equals salon.Id
                                  where bill.Pending == 1 && bill.CreatedDate > timeFrom
                                  && bill.IsDelete == 0 && (salonId == 0 || bill.SalonId == salonId)
                                  select new BillPendingItem
                                  {
                                      BillId = bill.Id,
                                      IsOwner = bill.StaffHairdresserId == staffId,
                                      Order = GetBillOrder(bill.BillCode, 4),
                                      CustomerId = bill.CustomerId ?? 0,
                                      CustomerName = customer.Fullname,
                                      CustomerPhone = customer.Phone,
                                      IsShineMember = customer.MemberType != null && customer.MemberEndTime != null && customer.MemberStartTime != null ?
                                        customer.MemberType == 1 && customer.MemberStartTime <= DateTime.Now && customer.MemberEndTime >= DateTime.Now : false,
                                      CreatedTime = String.Format("{0:HH}", bill.CreatedDate) + "h" + String.Format("{0:mm}", bill.CreatedDate),
                                      HasHistory = customer.DateLastBillService != null,
                                      WarningSkinner = DateTime.Now.Subtract(bill.CreatedDate.Value).TotalMinutes > 1 && bill.StaffHairMassageId == null
                                  }).ToListAsync();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private int GetBillOrder(string billCode, int orderLen)
        {
            if (billCode != null && billCode != "")
            {
                var code = billCode.Substring(billCode.Length - orderLen, orderLen);
                int order = int.TryParse(code, out order) ? order : 0;
                return order;
            }
            else
            {
                return 0;
            }
        }


        /// <summary>
        /// Get all bill pending of a skinner in salon today
        /// </summary>
        public async Task<List<BillPendingItem>> GetAllBillPendingOfSkinner(int staffId, int salonId, DateTime timeFrom)
        {
            try
            {
                var list = await (from bill in dbContextWrite.BillService
                                  join customer in dbContextWrite.Customer on bill.CustomerId equals customer.Id
                                  join salon in dbContextWrite.TblSalon on bill.SalonId equals salon.Id
                                  where bill.Pending == 1 && bill.CreatedDate > timeFrom
                                  && bill.IsDelete == 0 && bill.SalonId == salonId && bill.StaffHairMassageId == staffId
                                  select new BillPendingItem
                                  {
                                      BillId = bill.Id,
                                      IsOwner = true,
                                      Order = GetBillOrder(bill.BillCode, 4),
                                      CustomerId = bill.CustomerId ?? 0,
                                      CustomerName = customer.Fullname,
                                      CustomerPhone = customer.Phone,
                                      IsShineMember = customer.MemberType != null && customer.MemberEndTime != null && customer.MemberStartTime != null ?
                                        customer.MemberType == 1 && customer.MemberStartTime <= DateTime.Now && customer.MemberEndTime >= DateTime.Now : false,
                                      CreatedTime = string.Format("{0:HH}", bill.CreatedDate) + "h" + string.Format("{0:mm}", bill.CreatedDate),
                                      HasHistory = customer.DateLastBillService != null,
                                      WarningSkinner = DateTime.Now.Subtract(bill.CreatedDate.Value).TotalMinutes > 1 && bill.StaffHairMassageId == null
                                  }).ToListAsync();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get all bill pending of a stylist in salon today
        /// </summary>
        public async Task<List<BillPendingItem>> GetAllBillPendingOfStylist(int staffId, int salonId, DateTime timeFrom)
        {
            try
            {
                var list = await (from bill in dbContextWrite.BillService
                                  join customer in dbContextWrite.Customer on bill.CustomerId equals customer.Id
                                  join salon in dbContextWrite.TblSalon on bill.SalonId equals salon.Id
                                  where bill.Pending == 1 && bill.CreatedDate > timeFrom
                                  && bill.IsDelete == 0 && bill.SalonId == salonId && bill.StaffHairdresserId == staffId
                                  select new BillPendingItem
                                  {
                                      BillId = bill.Id,
                                      IsOwner = true,
                                      Order = GetBillOrder(bill.BillCode, 4),
                                      CustomerId = bill.CustomerId ?? 0,
                                      CustomerName = customer.Fullname,
                                      CustomerPhone = customer.Phone,
                                      IsShineMember = customer.MemberType != null && customer.MemberEndTime != null && customer.MemberStartTime != null ?
                                        customer.MemberType == 1 && customer.MemberStartTime <= DateTime.Now && customer.MemberEndTime >= DateTime.Now : false,
                                      CreatedTime = string.Format("{0:HH}", bill.CreatedDate) + "h" + string.Format("{0:mm}", bill.CreatedDate),
                                      HasHistory = customer.DateLastBillService != null,
                                      WarningSkinner = DateTime.Now.Subtract(bill.CreatedDate.Value).TotalMinutes > 1 && bill.StaffHairMassageId == null
                                  }).ToListAsync();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
