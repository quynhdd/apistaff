﻿using APIStaff.Models.Solution_30Shine;
using APIStaff.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIStaff.Repository.Implement
{
    public class DeviceRepo : IDeviceRepo
    {
        private readonly Solution_30shineContext dbContext;
        private readonly DbSet<Device> dbSet;
        //constructor
        public DeviceRepo(Solution_30shineContext dbContext)
        {
            this.dbContext = dbContext;
            dbSet = dbContext.Set<Device>();
        }

        public async Task<Device> GetDevice(string imei, int salonId)
        {
            try
            {
                return await dbSet.FirstOrDefaultAsync(w => w.ImeiOrMacIp == imei && w.OwnerId == salonId && w.OwnerType == 0);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
