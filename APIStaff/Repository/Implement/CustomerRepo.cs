﻿using APIStaff.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using read = APIStaff.Models.Solution_30Shine_BI;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using static APIStaff.Models.CustomeModel.OutputModel;
using APIStaff.Models.CustomeModel;

namespace APIStaff.Repository.Implement
{
    public class CustomerRepo : ICustomerRepo
    {
        private readonly read.Solution_30shineContext dbContext;
        private readonly DbSet<read.Customer> dbSet;
        //constructor
        public CustomerRepo(read.Solution_30shineContext dbContext)
        {
            this.dbContext = dbContext;
            dbSet = dbContext.Set<read.Customer>();
        }

        public async Task<List<read.DcustomerService>> GetListService(Expression<Func<read.DcustomerService, bool>> expression)
        {
            try
            {
                return await dbContext.DcustomerService.AsNoTracking().Where(expression).ToListAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<List<ProductData>> GetListProduct(List<int> CategoryId, int CustomerId)
        {
            try
            {
                return await (from a in dbContext.DcustomerProduct.AsNoTracking()
                              join b in dbContext.Product.AsNoTracking() on a.ProductId equals b.Id
                              where a.CustomerId == CustomerId && CategoryId.Contains(b.CategoryId.Value)
                              select new ProductData
                              {
                                  BillId = a.BillId,
                                  CategoryId = b.CategoryId.Value,
                                  DateOfBill = a.DateOfBill,
                                  ProductId = a.ProductId,
                                  ProductName = b.Name
                              }
                             ).ToListAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<read.DcustomerProduct> GetProduct(List<int> CategoryId, int CustomerId)
        {
            try
            {
                return await (from a in dbContext.DcustomerProduct
                              join b in dbContext.Product on a.ProductId equals b.Id
                              where a.CustomerId == CustomerId && CategoryId.Contains(b.CategoryId.Value)
                              select a
                             ).FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<read.Customer> GetById(int Id)
        {
            try
            {
                return await dbSet.FirstOrDefaultAsync(w => w.Id == Id && w.IsDelete == 0);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// get info special customer
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public async Task<SpecialCustomer> GetInfoSpecialCustomer(int customerId)
        {
            try
            {
                var data = await (from a in dbContext.SpecialCustomer
                                  join b in dbContext.SpecialCusDetail on a.Id equals b.SpecialCusId
                                  where a.IsDelete == false && a.CustomerId == customerId
                                  select new SpecialCustomer
                                  {
                                      Note = b.ReasonDiff
                                  }).FirstOrDefaultAsync();

                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
