﻿using APIStaff.Models.Solution_30Shine;
using APIStaff.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static APIStaff.Models.CustomeModel.OutputModel;

namespace APIStaff.Repository.Implement
{
    public class BillServiceHisRepo : IBillServiceHisRepo
    {
        // only read statistic
        private readonly Solution_30shineContext dbContext;
        private readonly DbSet<BillServiceHis> dbSet;

        public BillServiceHisRepo(Solution_30shineContext dbContext)
        {
            this.dbContext = dbContext;
            dbSet = dbContext.Set<BillServiceHis>();
        }

        public async Task<List<CustomerBill>> GetLatestCustomerBills(int customerId, int billCount)
        {
            try
            {
                List<CustomerBill> customerBills = new List<CustomerBill>();
                customerBills = await (from bill in dbContext.BillService
                                       join customer in dbContext.Customer
                                       on bill.CustomerId equals customer.Id
                                       where bill.IsDelete != 1 && bill.Pending != 1
                                       && bill.ServiceIds != "" && bill.ServiceIds != null && bill.Images != null && bill.Images != ""
                                       && customer.Id == customerId
                                       orderby bill.Id descending
                                       select new CustomerBill
                                       {
                                           CreatedTime = String.Format("{0:dd-MM-yyyy HH:mm:ss}", bill.CreatedDate),
                                           Services = JsonConvert.DeserializeObject<List<ServiceItem>>(bill.ServiceIds),
                                           ImageUrls = ConvertImageStringToImageUrls(bill.Images)
                                       }).Take(billCount).ToListAsync();
                return customerBills;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private List<string> ConvertImageStringToImageUrls(string imageString)
        {
            try
            {
                List<string> imageUrls = new List<string>();
                if (imageString != null) imageUrls = imageString.Split(',').ToList();
                return imageUrls;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
