﻿using read = APIStaff.Models.Solution_30Shine_BI;
using APIStaff.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using static APIStaff.Models.CustomeModel.OutputModel;

namespace APIStaff.Repository.Implement
{
    public class ServiceRepo : IServiceRepo
    {
        private readonly read.Solution_30shineContext dbContextRead;
        private readonly DbSet<read.Service> dbSetRead;

        public ServiceRepo(read.Solution_30shineContext dbContextRead)
        {
            this.dbContextRead = dbContextRead;
            dbSetRead = dbContextRead.Set<read.Service>();
        }

        public async Task<List<ServiceModel>> GetAll()
        {
            try
            {
                var list = await (from service in dbContextRead.Service
                                  where service.IsDelete == 0 && service.Publish == 1
                                  select new ServiceModel
                                  {
                                      Id = service.Id,
                                      Name = service.Name
                                  }).ToListAsync();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
