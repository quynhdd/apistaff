﻿using APIStaff.Models.Solution_30Shine;
using APIStaff.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace APIStaff.Repository.Implement
{
    public class FlowTimeKeepingRepo : IFlowTimeKeepingRepo
    {
        private readonly Solution_30shineContext dbContext;
        private readonly DbSet<FlowTimeKeeping> dbSet;
        //constructor
        public FlowTimeKeepingRepo(Solution_30shineContext dbContext)
        {
            this.dbContext = dbContext;
            dbSet = dbContext.Set<FlowTimeKeeping>();
        }

        public async Task<FlowTimeKeeping> Get(Expression<Func<FlowTimeKeeping, bool>> expression)
        {
            try
            {
                return await dbSet.FirstOrDefaultAsync(expression);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(FlowTimeKeeping record)
        {
            try
            {
                dbContext.Update(record);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task SaveChanges()
        {
            try
            {
                await dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
