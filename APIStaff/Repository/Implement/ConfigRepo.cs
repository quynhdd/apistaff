﻿using read = APIStaff.Models.Solution_30Shine_BI;
using APIStaff.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace APIStaff.Repository.Implement
{
    public class ConfigRepo : IConfigRepo
    {
        private readonly read.Solution_30shineContext dbContext;
        private readonly DbSet<read.TblConfig> dbSet;

        public ConfigRepo(read.Solution_30shineContext dbContext)
        {
            this.dbContext = dbContext;
            dbSet = dbContext.Set<read.TblConfig>();
        }
        public async Task<List<read.TblConfig>> GetList(Expression<Func<read.TblConfig, bool>> expression)
        {
            return await dbSet.Where(expression).ToListAsync();
        }
    }
}
