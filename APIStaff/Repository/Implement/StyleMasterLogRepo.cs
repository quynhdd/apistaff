﻿using APIStaff.Models.Solution_30Shine;
using Microsoft.EntityFrameworkCore;
using APIStaff.Repository.Interface;
using System.Threading.Tasks;
using System;
using System.Linq.Expressions;

namespace APIStaff.Repository.Implement
{
    public class StyleMasterLogRepo : IStyleMasterLogRepo
    {
        private readonly Solution_30shineContext dbContext;
        private readonly DbSet<StyleMasterLog> dbSet;

        public StyleMasterLogRepo(Solution_30shineContext dbContext)
        {
            this.dbContext = dbContext;
            dbSet = dbContext.Set<StyleMasterLog>();
        }

        public async Task<StyleMasterLog> Get(Expression<Func<StyleMasterLog, bool>> expression)
        {
            try
            {
                return await dbSet.FirstOrDefaultAsync(expression);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Remove(StyleMasterLog styleMasterLog)
        {
            try
            {
                dbSet.Remove(styleMasterLog);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void Add(StyleMasterLog styleMasterLog)
        {
            try
            {
                dbSet.Add(styleMasterLog);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task SaveChanges()
        {
            try
            {
                await dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
