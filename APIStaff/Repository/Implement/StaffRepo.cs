﻿using APIStaff.Models.Solution_30Shine;
using APIStaff.Models.CustomeModel;
using APIStaff.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Globalization;
using static APIStaff.Models.CustomeModel.OutputModel;
using System.Data;

namespace APIStaff.Repository.Implement
{
    public class StaffRepo : IStaffRepo
    {
        private readonly Solution_30shineContext dbContext;
        private readonly DbSet<Staff> dbSet;
        public CultureInfo culture = new CultureInfo("vi-VN");

        public StaffRepo(Solution_30shineContext dbContext)
        {
            this.dbContext = dbContext;
            dbSet = dbContext.Set<Staff>();
        }

        public async Task<Staff> Get(Expression<Func<Staff, bool>> expression)
        {
            try
            {
                return await dbSet.FirstOrDefaultAsync(expression);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<StaffInfor> GetStaffLogin(string email)
        {
            try
            {
                StaffInfor staffResponse = await (
                                                from staff in dbContext.Staff
                                                join salon in dbContext.TblSalon on staff.SalonId equals salon.Id into SalonGroup
                                                from first in SalonGroup.DefaultIfEmpty()
                                                join department in dbContext.StaffType on staff.Type equals department.Id into DepartmentGroup
                                                from second in DepartmentGroup.DefaultIfEmpty()
                                                join skillLevel in dbContext.TblSkillLevel on staff.SkillLevel equals skillLevel.Id into SkillGroup
                                                from third in SkillGroup.DefaultIfEmpty()
                                                where staff.Active == 1 && staff.Email == email
                                                select new StaffInfor
                                                {
                                                    StaffId = staff.Id,
                                                    FullName = staff.Fullname,
                                                    Password = staff.Password,
                                                    Permission = staff.Permission,
                                                    DepartmentId = staff.Type,
                                                    Department = second.Name,
                                                    DepartmentAlias = second.Meta,
                                                    SalonId = staff.SalonId,
                                                    SalonName = first.Name,
                                                    JoinDate = staff.DateJoin != null ? staff.DateJoin.Value.ToString("dd/MM/yyyy") : "01/01/0001",
                                                    BirthDay = staff.BirthDay != null ? staff.BirthDay.Value.ToString("dd/MM/yyyy") : "01/01/0001",
                                                    CMND = staff.StaffId,
                                                    Phone = staff.Phone,
                                                    Email = staff.Email,
                                                    Address = staff.Address,
                                                    SkillLevel = third.Name,
                                                    Gender = staff.Gender == 1 ? "Name" : staff.Gender == 2 ? "Nữ" : "Khác",
                                                }
                                            ).FirstOrDefaultAsync();
                return staffResponse;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<List<StaffMember>> getStaffMembersBySalonId(int salonId, DateTime workDate)
        {
            try
            {
                List<StaffMember> staffMembers = new List<StaffMember>();
                if (salonId != 0)
                {
                    staffMembers = await (from a in dbContext.FlowTimeKeeping
                                          join b in dbContext.Staff on a.StaffId equals b.Id
                                          where a.WorkDate == workDate && a.SalonId == salonId && a.IsEnroll == true
                                          select new StaffMember
                                          {
                                              Id = a.StaffId,
                                              FullName = b.Fullname,
                                              Code = b.Code,
                                              DepartmentId = b.Type,
                                          }).ToListAsync();
                }
                Console.WriteLine(staffMembers);
                return staffMembers;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<List<StylistBill>> GetStylistBills(int stylistId, DateTime from, DateTime to)
        {
            try
            {
                List<StylistBill> data = await (
                        from bill in dbContext.BillService
                        join customer in dbContext.Customer on bill.CustomerId equals customer.Id into gj
                        from customer in gj.DefaultIfEmpty()
                        join staff in dbContext.Staff on bill.StaffHairdresserId equals staff.Id into gj1
                        from staff in gj1.DefaultIfEmpty()
                        join sc in dbContext.ScscCheckError on bill.Id equals sc.BillServiceId into gj2
                        from sc in gj2.DefaultIfEmpty()
                        join cateShap in dbContext.ScscCategory on sc.ShapeId equals cateShap.IdScscCate into gj3
                        from cateShap in gj3.DefaultIfEmpty()
                        join cateConnect in dbContext.ScscCategory on sc.ConnectTiveId equals cateConnect.IdScscCate into gj4
                        from cateConnect in gj4.DefaultIfEmpty()
                        join cateSharpNess in dbContext.ScscCategory on sc.SharpNessId equals cateSharpNess.IdScscCate into gj5
                        from cateSharpNess in gj5.DefaultIfEmpty()
                        join cateSharCom in dbContext.ScscCategory on sc.ComPlatetionId equals cateSharCom.IdScscCate into gj6
                        from cateSharCom in gj6.DefaultIfEmpty()
                        join cateHairTip in dbContext.ScscCategory on sc.HairTipId equals cateHairTip.IdScscCate into gj7
                        from cateHairTip in gj7.DefaultIfEmpty()
                        join cateHairRoot in dbContext.ScscCategory on sc.HairRootId equals cateHairRoot.IdScscCate into gj8
                        from cateHairRoot in gj8.DefaultIfEmpty()
                        join cateHairWaves in dbContext.ScscCategory on sc.HairWavesId equals cateHairWaves.IdScscCate into gj9
                        from cateHairWaves in gj9.DefaultIfEmpty()
                        join img in dbContext.ImageData on bill.Id equals img.Objid into gj10
                        from img in gj10.DefaultIfEmpty()

                        where bill.StaffHairdresserId == stylistId
                           && bill.CreatedDate >= @from && bill.CreatedDate <= to
                           && bill.IsDelete != 1
                           && bill.Pending == 0
                           && bill.Images != null
                        // && !(img != null && (img.Slugkey != "image_bill_curling" || img.ImageBefore.Length <= 0 || img.ImageAfter.Length <= 0))

                        orderby bill.CreatedDate descending

                        select new StylistBill
                        {
                            Id = bill.Id,
                            ErrorNote = bill.ErrorNote,
                            Images = bill.Images,
                            // ImageStatusId = bill.ImageStatusId,
                            StaffHairdresserId = bill.StaffHairdresserId,
                            CreatedDate = bill.CreatedDate.GetValueOrDefault().ToString("dd-MM-yyyy"),
                            ImageChecked1 = bill.ImageChecked1 ?? "",
                            ImageChecked2 = bill.ImageChecked2 ?? "",
                            ImageChecked3 = bill.ImageChecked3 ?? "",
                            ImageChecked4 = bill.ImageChecked4 ?? "",
                            NoteByStylist = bill.NoteByStylist ?? "",
                            StylistName = staff.Fullname,
                            CustomerName = customer.Fullname,
                            CustomerPhone = customer.Phone,
                            ImageError = sc.ImageError ?? false,
                            Shape_ID = cateShap.ScscCateImageActive ?? "",
                            ConnectTive_ID = cateConnect.ScscCateImageActive ?? "",
                            SharpNess_ID = cateSharpNess.ScscCateImageActive ?? "",
                            ComPlatetion_ID = cateSharCom.ScscCateImageActive ?? "",
                            PointError = sc.PointError ?? 0,
                            SCSC_NoteError = sc.NoteError ?? "",
                            PassedStatus = sc.PointError == null ? 1 : (sc.PointError == 0 ? 2 : 3),
                            ImageCurlingBefore = img.ImageBefore ?? "",
                            ImageCurlingAfter = img.ImageAfter ?? "",
                            ImageCheckCurling1 = img.ImageCheckBefore ?? "",
                            ImageCheckCurling2 = img.ImageCheckAfter ?? "",
                            HairTip_ID = cateHairTip.ScscCateImageActive ?? "",
                            HairRoot_ID = cateHairRoot.ScscCateImageActive ?? "",
                            HairWaves_ID = cateHairWaves.ScscCateImageActive ?? "",
                            TotalPointSCSCCurling = sc.TotalPointScsccurling ?? 0
                        }
                    ).ToListAsync();
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public async Task<object> GetStylistCutQualityScore(int stylistId, DateTime from, DateTime to)
        {
            try
            {

                var cmd = dbContext.Database.GetDbConnection().CreateCommand();
                cmd.CommandText = "StoreStatistic_SCSC_Stylist";
                cmd.CommandType = CommandType.StoredProcedure;
                //cmd.Parameters.Add("@TimeFrom", from);
                cmd.Parameters.Add(new SqlParameter("@TimeFrom", from));
                cmd.Parameters.Add(new SqlParameter("@TimeTo", to));
                cmd.Parameters.Add(new SqlParameter("@StylistID", stylistId));
                int itemCount = 0;
                dbContext.Database.OpenConnection();
                var reader = cmd.ExecuteReader();
                object data = null;
                while (reader.Read())
                {
                    //var item = new ServiceData();
                    itemCount++;
                    data = new
                    {
                        AverageScore = reader["PointSCSC_TB"],
                        PercentError = reader["Bill_Loi"],
                        objStatistic = new
                        {
                            Stylist_ID = reader["Stylist_ID"],
                            TotalBill = reader["TotalBill"],
                            TotalBillAnhThieu_MoLech = reader["TotalBillAnhThieu_MoLech"],
                            TotalBillChuaDanhGia = reader["TotalBillChuaDanhGia"],
                            TotalBill_Error_ShapID = reader["TotalBill_Error_ShapID"],
                            TotalBill_Error_ConnectTive_ID = reader["TotalBill_Error_ConnectTive_ID"],
                            TotalBill_Error_SharpNess_ID = reader["TotalBill_Error_SharpNess_ID"],
                            TotalBill_Error_ComPlatetion_ID = reader["TotalBill_Error_ComPlatetion_ID"]
                        }

                    };
                    if (itemCount == 1) break;

                }
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public async Task SaveChanges()
        {
            try
            {
                await dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}