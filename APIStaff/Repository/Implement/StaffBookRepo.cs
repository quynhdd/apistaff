﻿using APIStaff.Models.Solution_30Shine;
using APIStaff.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static APIStaff.Models.CustomeModel.OutputModel;

namespace APIStaff.Repository.Implement
{
    public class StaffBookRepo : IStaffBookRepo
    {
        private readonly Solution_30shineContext dbContext;
        private readonly DbSet<StaffProcedure> dbSet;

        public StaffBookRepo(Solution_30shineContext dbContext)
        {
            this.dbContext = dbContext;
            dbSet = dbContext.Set<StaffProcedure>();
        }

        public async Task<List<StaffBook>> GetStaffBook(int? departmentId)
        {
            try
            {
                var list = await (from a in dbContext.StaffProcedure
                                  where a.IsDelete == false && a.Publish == true && a.DepartmentId == departmentId
                                  orderby a.Order ascending
                                  select new StaffBook
                                  {
                                      Id = a.Id,
                                      Title = a.Title,
                                      Images = JsonConvert.DeserializeObject<List<Image>>(a.Images)
                                  }
                            ).ToListAsync();
                return list;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
