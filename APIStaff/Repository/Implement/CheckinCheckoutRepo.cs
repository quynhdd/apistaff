﻿using APIStaff.Models.Solution_30Shine;
using APIStaff.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace APIStaff.Repository.Implement
{
    public class CheckinCheckoutRepo : ICheckinCheckoutRepo
    {
        private readonly Solution_30shineContext dbContext;
        private readonly DbSet<CheckinCheckout> dbSet;

        public CheckinCheckoutRepo(Solution_30shineContext dbContext)
        {
            this.dbContext = dbContext;
            dbSet = dbContext.Set<CheckinCheckout>();
        }

        /// <summary>
        /// Check checkin checkout status of staff today
        /// </summary>
        /// <returns>
        /// Staff not checkin : False
        /// Staff checkin but not checkout : True
        /// Staff has checkin and checkout : False
        /// </returns>
        public async Task<bool> CheckCheckinStatus(int staffId)
        {
            try
            {
                var record = await dbSet.FirstOrDefaultAsync(a => a.IsDelete == false && a.WorkDate == DateTime.Today && a.StaffId == staffId);
                return record != null && record.CheckoutTime == null;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<CheckinCheckout> Get(Expression<Func<CheckinCheckout, bool>> expression)
        {
            try
            {
                return await dbSet.FirstOrDefaultAsync(expression);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Add(CheckinCheckout record)
        {
            try
            {
                dbSet.Add(record);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void Update(CheckinCheckout record)
        {
            try
            {
                dbContext.Update(record);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task SaveChanges()
        {
            try
            {
                await dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
