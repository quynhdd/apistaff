﻿using read = APIStaff.Models.Solution_30Shine_BI;
using APIStaff.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using static APIStaff.Models.CustomeModel.OutputModel;

namespace APIStaff.Repository.Implement
{
    public class ProductRepo : IProductRepo
    {
        private readonly read.Solution_30shineContext dbContextRead;
        private readonly DbSet<read.Product> dbSetRead;

        public ProductRepo(read.Solution_30shineContext dbContextRead)
        {
            this.dbContextRead = dbContextRead;
            dbSetRead = dbContextRead.Set<read.Product>();
        }

        public async Task<List<ProductModel>> GetAll()
        {
            try
            {
                var list = await (from product in dbContextRead.Product
                                  where product.IsDelete == 0 && product.Publish == 1 && product.CategoryId != 98
                                  select new ProductModel
                                  {
                                      Id = product.Id,
                                      Name = product.Name
                                  }).ToListAsync();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
