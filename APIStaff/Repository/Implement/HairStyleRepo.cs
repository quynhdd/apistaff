﻿using APIStaff.Models.Solution_30Shine;
using APIStaff.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using static APIStaff.Models.CustomeModel.OutputModel;
using Newtonsoft.Json;

namespace APIStaff.Repository.Implement
{
    public class HairStyleRepo : IHairStyleRepo
    {

        private readonly Solution_30shineContext dbContext;
        private readonly DbSet<ApiHairMode> dbSet;

        public HairStyleRepo(Solution_30shineContext dbContext)
        {
            this.dbContext = dbContext;
            dbSet = dbContext.Set<ApiHairMode>();
        }

        public async Task<List<HairStyle>> GetHairStyle()
        {
            try
            {
                var list = await (from a in dbContext.ApiHairMode
                                  where a.IsDelete == false && a.Publish == true && a.Id != 15 && a.Id != 16
                                  select new HairStyle
                                  {
                                      Id = a.Id,
                                      Title = a.Title,
                                      Description = a.Description,
                                      Images = JsonConvert.DeserializeObject<List<Image>>(a.Images)
                                  }
                            ).ToListAsync();
                return list;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
