﻿using APIStaff.Models.Solution_30Shine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIStaff.Repository.Interface
{
    public interface IWorkTimeRepo
    {
        Task<WorkTime> GetById(int workTimeId);
    }
}
