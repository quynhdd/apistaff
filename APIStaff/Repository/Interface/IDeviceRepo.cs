﻿using APIStaff.Models.Solution_30Shine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIStaff.Repository.Interface
{
    public interface IDeviceRepo
    {
        Task<Device> GetDevice(string imei, int salonId);
    }
}
