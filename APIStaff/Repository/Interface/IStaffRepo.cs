﻿using APIStaff.Models.Solution_30Shine;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Collections.Generic;
using System;
using static APIStaff.Models.CustomeModel.OutputModel;

namespace APIStaff.Repository.Interface
{
    public interface IStaffRepo
    {
        Task<List<StylistBill>> GetStylistBills(int stylistId, DateTime from, DateTime to);
        Task<Staff> Get(Expression<Func<Staff, bool>> expression);
        Task<StaffInfor> GetStaffLogin(string email);
        Task<List<StaffMember>> getStaffMembersBySalonId(int salonId, DateTime workDate);
		Task<object> GetStylistCutQualityScore(int stylistId, DateTime from, DateTime to);
        Task SaveChanges();
    }
}
