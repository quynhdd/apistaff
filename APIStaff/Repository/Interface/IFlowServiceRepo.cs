﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static APIStaff.Models.CustomeModel.OutputModel;

namespace APIStaff.Repository.Interface
{
    public interface IFlowServiceRepo
    {
        Task<List<ServiceData>> GetServiceData(int billId);
        Task<List<ServiceData>> CountServiceData(int CustomerId);
    }
}
