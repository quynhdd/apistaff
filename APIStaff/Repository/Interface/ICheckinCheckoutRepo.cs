﻿using APIStaff.Models.Solution_30Shine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace APIStaff.Repository.Interface
{
    public interface ICheckinCheckoutRepo
    {
        Task<bool> CheckCheckinStatus(int staffId);
        Task<CheckinCheckout> Get(Expression<Func<CheckinCheckout, bool>> expression);
        void Add(CheckinCheckout record);
        void Update(CheckinCheckout record);
        Task SaveChanges();
    }
}
