﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using static APIStaff.Models.CustomeModel.OutputModel;

namespace APIStaff.Repository.Interface
{
    public interface IProductRepo
    {
        Task<List<ProductModel>> GetAll();
    }
}
