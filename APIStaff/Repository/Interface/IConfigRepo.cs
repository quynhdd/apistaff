﻿using read=APIStaff.Models.Solution_30Shine_BI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace APIStaff.Repository.Interface
{
    public interface IConfigRepo
    {
        Task<List<read.TblConfig>> GetList(Expression<Func<read.TblConfig, bool>> expression);
    }
}
