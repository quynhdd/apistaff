﻿using read = APIStaff.Models.Solution_30Shine_BI;
using write = APIStaff.Models.Solution_30Shine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using static APIStaff.Models.CustomeModel.OutputModel;

namespace APIStaff.Repository.Interface
{
    public interface IBillServiceRepo
    {
        Task<read.BillService> GetDbRead(Expression<Func<read.BillService, bool>> expression);
        Task<write.BillService> GetDbWrite(Expression<Func<write.BillService, bool>> expression);
        Task<DetailBillPendingStatus> GetDetailBillPendingStatus(int billId);
        Task<List<BillPendingItem>> GetAllBillPending(int staffId, int salonId, DateTime timeFrom);
        Task<List<BillPendingItem>> GetAllBillPendingOfSkinner(int staffId, int salonId, DateTime timeFrom);
        Task<List<BillPendingItem>> GetAllBillPendingOfStylist(int staffId, int salonId, DateTime timeFrom);
        Task<Tuple<double, bool>> GetWaitTimeSinceLastService(int skinnerId);
        Task<bool> Update(write.BillService bill);
    }
}
