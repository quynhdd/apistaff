﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static APIStaff.Models.CustomeModel.OutputModel;

namespace APIStaff.Repository.Interface
{
    public interface IFavoriteProductServiceRepo
    {
        Task<List<ProductModel>> GetFavoriteProduct(int staffId);
        Task<List<ServiceModel>> GetFavoriteService(int staffId);
    }
}
