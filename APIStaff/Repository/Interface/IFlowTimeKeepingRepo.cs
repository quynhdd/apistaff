﻿using APIStaff.Models.Solution_30Shine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace APIStaff.Repository.Interface
{
    public interface IFlowTimeKeepingRepo
    {
        Task<FlowTimeKeeping> Get(Expression<Func<FlowTimeKeeping, bool>> expression);
        void Update(FlowTimeKeeping record);
        Task SaveChanges();
    }
}
