﻿using read = APIStaff.Models.Solution_30Shine_BI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using static APIStaff.Models.CustomeModel.OutputModel;

namespace APIStaff.Repository.Interface
{
    public interface ICustomerRepo
    {
        Task<List<read.DcustomerService>> GetListService(Expression<Func<read.DcustomerService, bool>> expression);
        Task<List<ProductData>> GetListProduct(List<int> CategoryId, int CustomerId);
        Task<read.DcustomerProduct> GetProduct(List<int> CategoryId, int CustomerId);
        Task<read.Customer> GetById(int Id);
        Task<SpecialCustomer> GetInfoSpecialCustomer(int customerId);
    }
}
