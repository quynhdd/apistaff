﻿using APIStaff.Models.Solution_30Shine;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using static APIStaff.Models.CustomeModel.OutputModel;

namespace APIStaff.Repository.Interface
{
    public interface IStyleMasterRepo
    {
        Task<List<MyStyleMasterPost>> GetPostByStatus(int staffId, int status);
        Task<List<StyleMasterPost>> GetPostForCustomerByTimespan(int customerId, DateTime TimeFrom, DateTime TimeTo);
        Task<List<StyleMasterPost>> GetPostForStaffByTimespan(int staffId, DateTime TimeFrom, DateTime TimeTo);
        Task<List<StyleMasterPost>> GetPostForCustomerPagging(int customerId, DateTime timeFrom, DateTime timeTo, int pageIndex, int pageSize);
        Task<List<StyleMasterPost>> GetPostForStaffPagging(int staffId, DateTime timeFrom, DateTime timeTo, int pageIndex, int pageSize);
        Task<List<StyleMasterPost>> GetTopPostByTimespan(DateTime timeFrom, DateTime timeTo, int top);
        Task<List<StyleMasterPost>> GetNearlyLikePost(int customerId, int top);
        Task<StyleMaster> GetPostByPostNumber(string postnumber);
        Task SaveChanges();
        void Update(StyleMaster styleMaster);
    }
}
