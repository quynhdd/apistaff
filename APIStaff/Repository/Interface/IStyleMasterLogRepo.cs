﻿using APIStaff.Models.Solution_30Shine;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace APIStaff.Repository.Interface
{
    public interface IStyleMasterLogRepo
    {
        Task<StyleMasterLog> Get(Expression<Func<StyleMasterLog, bool>> expression);
        Task SaveChanges();
        void Add(StyleMasterLog styleMasterLog);
        void Remove(StyleMasterLog styleMasterLog);
    }
}
