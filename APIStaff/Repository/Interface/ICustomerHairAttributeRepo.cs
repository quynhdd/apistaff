﻿using read = APIStaff.Models.Solution_30Shine_BI;
using write = APIStaff.Models.Solution_30Shine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIStaff.Repository.Interface
{
    public interface ICustomerHairAttributeRepo
    {
        Task<write.CustomerHairAttribute> GetByBillId(int BillId);

        void Update(write.CustomerHairAttribute record);
        void Add(write.CustomerHairAttribute record);
        Task SaveChangeAsync();
    }
}
