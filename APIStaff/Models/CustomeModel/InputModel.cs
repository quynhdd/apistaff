﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIStaff.Models.CustomeModel
{
    public class InputModel
    {
        #region Query params
        public class QueryStylist
        {
            public int SalonId { get; set; }
            public string WorkDate { get; set; }
        }

        public class QueryBookHour
        {
            public int SalonId { get; set; }
            public int StylistId { get; set; }
            public string DateBook { get; set; }
            public string CustomerPhone { get; set; }
        }

        public class QueryMenbershipRanking
        {
            public string Phone { get; set; }
            public string From { get; set; }
            public string To { get; set; }
        }

        public class QueryBooking
        {
            public string Phone { get; set; }
            public string BookDate { get; set; }
        }
        public class QueryHourBooking
        {
            public int hourId { get; set; }
            public TimeSpan? hourFram { get; set; }
        }

        #endregion

        #region Request header
        public class ReqHeadCustomerLogin
        {
            public string CustomerPhone { get; set; }
            public string DeviceId { get; set; }
            public string Ip { get; set; }
            public string AppId { get; set; }
            public string AppVersion { get; set; }
        }

        #endregion

        #region Request data


        public class ReqDataCustomer
        {
            public string Phone { get; set; }
            public string Name { get; set; }
            public string Dob { get; set; }
            public string Email { get; set; }
            public int SalonId { get; set; }
            public string Address { get; set; }
            public string Avatar { get; set; }
            public string FacebookId { get; set; }
            public string FacebookName { get; set; }
        }

        public class UpdateBillBody
        {
            public int SkinnerId { get; set; }
            public int StylistId { get; set; }
            public string NoteLessThanMinutes { get; set; }
        }

        public class ReqDataOTP
        {
            public string Phone { get; set; }
            public string Code { get; set; }
        }

        public class ReqDataBooking
        {
            public string Phone { get; set; }
            public int SalonId { get; set; }
            public int StylistId { get; set; }
            public string DateBook { get; set; }
            public int HourBookId { get; set; }
            public string NoteBeforBook { get; set; }
            public string NoteAfterBook { get; set; }

            //(timeline: 0; web: 1; app customer: 2)
            public string BookAt { get; set; }
        }
        /// <summary>
        /// Input salary add
        /// author: dungnm
        /// </summary>
        public class ReqSalaryIncome
        {
            public string WorkDate { get; set; }
            public int SalonId { get; set; }
            public string StaffId { get; set; }
            public int WorkHour { get; set; }
        }

        /// <summary>
        /// Use erp
        /// </summary>
        public class ReqDeleteTimekeeping
        {
            public int StaffId { get; set; }
            public int SalonId { get; set; }
            public string workDate { get; set; }
            public bool IsEnroll { get; set; }
            public int workTimeId { get; set; }
            public int WorkHour { get; set; }
        }

        /// <summary>
        /// Use app mobile
        /// </summary>
        public class ReqDeleteTimekeepingByApp
        {
            public string StaffId { get; set; }
            public int SalonId { get; set; }
            public string workDate { get; set; }
        }

        /// <summary>
        /// Use app mobile
        /// </summary>
        public class ReqCustomerHairAttribute
        {
            public int UserId { get; set; }
            public int CustomerId { get; set; }
            public int BillId { get; set; }
            public string Note { get; set; }
        }

        /// <summary>
        /// Use app customer
        /// </summary>
        public class ReqUpdateVoteStyleMaster
        {
            public string PostNumber { get; set; }
            public int Like { get; set; }
            public int UserId { get; set; }
            public int UserType { get; set; }
        }

        public class RequestCheckinCheckout
        {
            public int StaffId { get; set; }
            public string CheckinCheckoutDatetime { get; set; }
            public int SalonId { get; set; }
            public int Type { get; set; }
            public string Imei { get; set; }
            public string UniqueKey { get; set; }
        }

        // login
        public class Account
        {
            public string email { get; set; }
            public string password { get; set; }
        }

        public class ChangePasswordRequest
        {
            public string OldPassword { get; set; }
            public string NewPassword { get; set; }
        }

        #endregion

        #region Response data
        public class ResDataUser
        {
            public int Id { get; set; }
            public string Account { get; set; }
            public string Password { get; set; }
            public string Name { get; set; }
            public string Email { get; set; }
            public string Phone { get; set; }
        }


        #endregion

        #region[Authen]
        public class AuthenModel
        {
            public string Phone { get; set; }
            public string DeviceId { get; set; }
            public string Ip { get; set; }
            public string AppId { get; set; }
            public string AppVersion { get; set; }
            public ResDataUser User { get; set; }
            public object Perm { get; set; }
        }
        #endregion
    }
}
