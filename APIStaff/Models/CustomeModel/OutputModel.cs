﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIStaff.Models.CustomeModel
{
    public class OutputModel
    {
        /// <summary>
        /// Response
        /// </summary>
        public class Response
        {
            public bool Status { get; set; }
            public string Message { get; set; }
            public dynamic Data { get; set; }
            public Response()
            {
                Status = false;
                Message = "";
                Data = null;
            }
        }

        /// <summary>
        /// get services data for history customer
        /// </summary>
        public class OutputHistoryBillService
        {
            public List<ServiceData> Service { get; set; }
            public int ServiceTDC { get; set; }
            public int ServiceDMN { get; set; }
        }

        /// <summary>
        /// get service data for history customer
        /// </summary>
        public class ServiceData
        {
            public string Name { get; set; }
            public int Quantity { get; set; }
            public int ServiceId { get; set; }
        }

        /// <summary>
        /// get offer for customer
        /// </summary>
        public class ServiceCustomer
        {
            public List<string> Offer { get; set; }
            public List<string> NotOffer { get; set; }
            public int QuantityUseService { get; set; }
            public string LastTimeUseHairColor { get; set; }
            public string LastTimeUseHairCurling { get; set; }
            public string LastTimeBuyProduct { get; set; }
            public List<string> LastBuyProducts { get; set; }
            public string LastTimeBuyFMCG { get; set; }
            public List<string> LastBuyFMCG { get; set; }
            public string LastTimeBuyUnderwear { get; set; }
            public List<string> LastBuyUnderwear { get; set; }
            public string NoteOfStaff { get; set; }
        }

        public class StylistBill
        {
            public int Id { get; set; }
            public string ErrorNote;
            public string Images;
            // spublic string ImageStatusId;
            public int? StaffHairdresserId;
            public string CreatedDate;
            public string ImageChecked1;
            public string ImageChecked2;
            public string ImageChecked3;
            public string ImageChecked4;
            public string NoteByStylist;
            public string StylistName;
            public string CustomerName;
            public string CustomerPhone;
            public string Shape_ID;
            public string ConnectTive_ID;
            public string SharpNess_ID;
            public string ComPlatetion_ID;
            public int PointError;
            public bool ImageError;
            public string SCSC_NoteError;
            public string ImageCurlingBefore;
            public string ImageCurlingAfter;
            public string ImageCheckCurling1;
            public string ImageCheckCurling2;
            public int PassedStatus;
            public string HairTip_ID;
            public string HairRoot_ID;
            public string HairWaves_ID;
            public int TotalPointSCSCCurling;
        }

        /// <summary>
        /// get service data for history customer
        /// </summary>
        public class ProductData
        {
            public int ProductId { get; set; }
            public string ProductName { get; set; }
            public DateTime DateOfBill { get; set; }
            public int BillId { get; set; }
            public int CategoryId { get; set; }
        }

        /// <summary>
        /// BillInfo
        /// </summary>
        public class BillInfo
        {
            public int CustomerId { get; set; }
            public DateTime TimeInBill { get; set; }
            public DateTime CompleteTimeBill { get; set; }
            public bool IsImage { get; set; }
        }

        /// <summary>
        /// StyleMaster Post
        /// </summary>
        public class StyleMasterPost
        {
            public string PostNumber { get; set; }
            public string StylistName { get; set; }
            public bool IsLiked { get; set; }
            public int? TotalLikes { get; set; }
            public string Img_1 { get; set; }
            public string Img_2 { get; set; }
            public string Img_3 { get; set; }
            public string Img_4 { get; set; }
            public bool UseCurling { get; set; }
            public bool UseHairDye { get; set; }
        }

        /// <summary>
        /// StyleMaster Post
        /// </summary>
        public class MyStyleMasterPost
        {
            public string PostNumber { get; set; }
            public int? TotalLikes { get; set; }
            public string Img_1 { get; set; }
            public string Img_2 { get; set; }
            public string Img_3 { get; set; }
            public string Img_4 { get; set; }
            public string Date { get; set; }
        }

        /// <summary>
        /// Top StyleMaster Post Response
        /// </summary>
        public class TopStyleMasterResponse
        {
            public string PostOfMonth { get; set; }
            public List<StyleMasterPost> Origin { get; set; }
        }

        /// <summary>
        /// Staff Book
        /// </summary>
        public class StaffBook
        {
            public int Id { get; set; }
            public string Title { get; set; }
            public List<Image> Images { get; set; }
        }

        /// <summary>
        /// Hair Style
        /// </summary>
        public class HairStyle
        {
            public int Id { get; set; }
            public string Title { get; set; }
            public string Description { get; set; }
            public List<Image> Images { get; set; }
        }

        /// <summary>
        /// Image
        /// </summary>
        public class Image
        {
            public string Url { get; set; }
            public string Thumb { get; set; }
        }

        /// <summary>
        /// Detail bill pending status
        /// </summary>
        public class DetailBillPendingStatus
        {
            public int BillId { get; set; }
            public int? StylistId { get; set; }
            public string StylistName { get; set; }
            public string StylistCode { get; set; }
            public int? SkinnerId { get; set; }
            public string SkinnerName { get; set; }
            public string SkinnerCode { get; set; }
            public int? EstimateTimeCut { get; set; }
            public bool IsSulphite { get; set; }
            public string SkinnerBoundTime { get; set; }
        }

        /// <summary>
        /// Bill pending item
        /// </summary>
        public class BillPendingItem
        {
            public int BillId { get; set; }

            /// isOwner : Bill có phải của Stylist truyền vào hay không
            public bool IsOwner { get; set; }

            /// Số thứ tự bill
            public int Order { get; set; }

            public int CustomerId { get; set; }

            public string CustomerName { get; set; }

            public string CustomerPhone { get; set; }

            public bool IsShineMember { get; set; }

            /// Thời gian tạo bill (tính theo giờ, ví dụ : 10h30)
            public string CreatedTime { get; set; }

            /// Đã có bill lịch sử hay chưa
            public bool HasHistory { get; set; }

            /// Cảnh báo skinner đã nhập mã hay chưa
            public bool WarningSkinner { get; set; }
        }
        public class StaffMember
        {
            public int Id { get; set; }
            public int? DepartmentId { get; set; }
            public string FullName { get; set; }
            public string Code { get; set; }
        }

        public class ServiceItem
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }
        public class CustomerBill
        {
            public String CreatedTime { get; set; }
            public List<ServiceItem> Services { get; set; }
            public List<string> ImageUrls { get; set; }
        }

        // login response
        public class LoginResponse
        {
            public LoginResponse(string accessToken, string refreshToken, string idToken, StaffInfor staffInfor)
            {
                AccessToken = accessToken;
                RefreshToken = refreshToken;
                IdToken = idToken;
                StaffInfor = staffInfor;
            }
            public string AccessToken { get; set; }
            public string RefreshToken { get; set; }
            public string IdToken { get; set; }
            public StaffInfor StaffInfor { get; set; }
        }

        public class StaffInfor
        {
            public int? StaffId { get; set; }
            public string FullName { get; set; }
            [JsonIgnore]
            public string Password { get; set; }
            public string Permission { get; set; }
            public int? DepartmentId { get; set; }
            public string Department { get; set; }
            /// Alias tên bộ phận (phục vụ hiển thị file pdf quy trình / quy định)
            public string DepartmentAlias { get; set; }
            public int? SalonId { get; set; }
            public string SalonName { get; set; }
            public string JoinDate { get; set; }
            public string Gender { get; set; }
            public string BirthDay { get; set; }
            public string CMND { get; set; }
            public string Phone { get; set; }
            public string Email { get; set; }
            public string Address { get; set; }
            public string SkillLevel { get; set; }
        }

        public class ServiceModel
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }

        public class ProductModel
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }

        /// <summary>
        /// Get info special customer
        /// </summary>
        public class SpecialCustomer
        {
            public string Note { get; set; }
        }
    }
}
