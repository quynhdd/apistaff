﻿using System;
using System.Collections.Generic;

namespace APIStaff.Models.Solution_30Shine
{
    public partial class StaffFavoriteProductService
    {
        public int Id { get; set; }
        public int StaffId { get; set; }
        public int ProductServiceId { get; set; }
        public int Type { get; set; }
    }
}
