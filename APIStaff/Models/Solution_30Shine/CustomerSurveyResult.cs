﻿using System;
using System.Collections.Generic;

namespace ApiManageApp.Models.Solution_30Shine
{
    public partial class CustomerSurveyResult
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool IsDelete { get; set; }
    }
}
