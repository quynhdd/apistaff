﻿using System;
using System.Collections.Generic;

namespace APIStaff.Models.Solution_30Shine
{
    public partial class PreviewImagesReport
    {
        public int Id { get; set; }
        public string ReportType { get; set; }
        public bool? IsDelete { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
