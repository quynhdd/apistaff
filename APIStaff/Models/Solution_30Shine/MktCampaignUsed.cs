﻿using System;
using System.Collections.Generic;

namespace APIStaff.Models.Solution_30Shine
{
    public partial class MktCampaignUsed
    {
        public int CampaignId { get; set; }
        public int TotalUsed { get; set; }
    }
}
