﻿using Amazon.DynamoDBv2.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIStaff.Models.Dynamo
{
    public class SurveyUser
    {
        [DynamoDBHashKey]
        public int UserId { get; set; }

        [DynamoDBRangeKey]
        public int UserType { get; set; }

        [DynamoDBProperty]
        public string QuestionId { get; set; }

        [DynamoDBProperty]
        public bool IsAnswer { get; set; }

        [DynamoDBProperty]
        public bool IsDelete { get; set; }

        [DynamoDBProperty]
        public DateTime CreatedDate { get; set; }

        [DynamoDBProperty]
        public DateTime? ModifiedDate { get; set; }
    }
}
