﻿using System;
using System.Collections.Generic;

namespace APIStaff.Models.Solution_30Shine_BI
{
    public partial class MktCampaignCustomer
    {
        public int Id { get; set; }
        public int CampaignId { get; set; }
        public int CustomerId { get; set; }
        public int Used { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public bool IsDelete { get; set; }
        public int? VoucherId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
