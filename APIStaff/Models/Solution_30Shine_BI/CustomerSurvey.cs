﻿using System;
using System.Collections.Generic;

namespace ApiManageApp.Models.Solution_30Shine_BI
{
    public partial class CustomerSurvey
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public int? ParentId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool IsDelete { get; set; }
    }
}
