﻿using System;
using System.Collections.Generic;

namespace APIStaff.Models.Solution_30Shine_BI
{
    public partial class MktCampaignUsed
    {
        public int CampaignId { get; set; }
        public int TotalUsed { get; set; }
    }
}
