﻿using System;
using System.Collections.Generic;

namespace APIStaff.Models.Solution_30Shine_BI
{
    public partial class ApiNotiSendManager
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public string DeviceRegistrationToken { get; set; }
        public DateTime? SendTime { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
