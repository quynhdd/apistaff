﻿using System;
using System.Collections.Generic;

namespace APIStaff.Models.Solution_30Shine_BI
{
    public partial class ServiceRatingRelationship
    {
        public int Id { get; set; }
        public int? BillServiceId { get; set; }
        public int? ServiceRatingId { get; set; }
        public int? SalonId { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
