﻿using System;
using System.Collections.Generic;

namespace APIStaff.Models.Solution_30Shine_BI
{
    public partial class MktVoucher
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int CampaignId { get; set; }
        public bool IsDelete { get; set; }
        public bool? IsActive { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
    }
}
