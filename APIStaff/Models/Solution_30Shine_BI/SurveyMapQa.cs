﻿using System;
using System.Collections.Generic;

namespace APIStaff.Models.Solution_30Shine_BI
{
    public partial class SurveyMapQa
    {
        public int Id { get; set; }
        public int? QuestionId { get; set; }
        public int? AnswerId { get; set; }
        public bool? IsDelete { get; set; }
        public bool? Publish { get; set; }
        public int? Order { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
