﻿using System;
using System.Collections.Generic;

namespace APIStaff.Models.Solution_30Shine_BI
{
    public partial class MapDeviceOwner
    {
        public int Id { get; set; }
        public int? OwnerId { get; set; }
        public int? DeviceId { get; set; }
        public bool? IsDelete { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
