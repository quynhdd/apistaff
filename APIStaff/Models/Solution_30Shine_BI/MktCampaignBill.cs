﻿using System;
using System.Collections.Generic;

namespace APIStaff.Models.Solution_30Shine_BI
{
    public partial class MktCampaignBill
    {
        public int Id { get; set; }
        public int CampaignId { get; set; }
        public int BillId { get; set; }
        public double DiscountMoney { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public bool IsDelete { get; set; }
        public int ServiceId { get; set; }
        public int? MoneyPrePaid { get; set; }
        public int? MoneyDeductions { get; set; }
    }
}
