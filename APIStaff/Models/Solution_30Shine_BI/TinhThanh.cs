﻿using System;
using System.Collections.Generic;

namespace APIStaff.Models.Solution_30Shine_BI
{
    public partial class TinhThanh
    {
        public int Id { get; set; }
        public string TenTinhThanh { get; set; }
        public int? VungMienId { get; set; }
        public int? ThuTu { get; set; }
        public int? TrangThai { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? Uid { get; set; }
        public byte? MigrateStatus { get; set; }
    }
}
