﻿using APIStaff.Extentions;
using APIStaff.Models.Solution_30Shine;
using APIStaff.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static APIStaff.Models.CustomeModel.OutputModel;

namespace APIStaff.Service
{
    public class BillService
    {
        private readonly IBillServiceRepo BillServiceRepo;
        private readonly IFlowServiceRepo FlowServiceRepo;
        private readonly ICustomerRepo CustomerRepo;
        private readonly IConfigRepo ConfigRepo;
        private readonly ICustomerHairAttributeRepo CustomerHairAttributeRepo;
        
        private readonly IFlowTimeKeepingRepo FlowTimeKeepingRepo;

        public BillService(IBillServiceRepo billServiceRepo,
                          IFlowServiceRepo flowServiceRepo,
                          ICustomerRepo customerRepo,
                          IConfigRepo configRepo,
                          ICustomerHairAttributeRepo customerHairAttributeRepo,
                          IFlowTimeKeepingRepo flowTimeKeepingRepo)
        {
            BillServiceRepo = billServiceRepo;
            FlowServiceRepo = flowServiceRepo;
            CustomerRepo = customerRepo;
            ConfigRepo = configRepo;
            CustomerHairAttributeRepo = customerHairAttributeRepo;
            FlowTimeKeepingRepo = flowTimeKeepingRepo;
        }

        /// <summary>
        /// Get history billservice
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <returns></returns>
        public async Task<OutputHistoryBillService> GetHistoryBillService(int CustomerId)
        {
            try
            {
                var record = new OutputHistoryBillService();
                record.Service = new List<ServiceData>();
                DateTime date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 7, 0, 0);
                var date2 = date.AddDays(1);
                //get bill pending
                var billPending = await BillServiceRepo.GetDbRead(w => w.CustomerId == CustomerId && w.Pending == 1 && w.IsDelete == 0 && w.CreatedDate >= date && w.CreatedDate < date2);
                if (billPending != null)
                {
                    //get bill completed
                    var billOld = await BillServiceRepo.GetDbRead(w => w.CustomerId == CustomerId && w.Pending == 0 && w.IsDelete == 0);
                    if (billOld != null)
                    {
                        //get service in bill
                        var Service = await FlowServiceRepo.GetServiceData(billOld.Id);
                        //count tdc and dmn
                        var CountService = await FlowServiceRepo.CountServiceData(CustomerId);
                        record = new OutputHistoryBillService()
                        {
                            Service = Service,
                            ServiceDMN = CountService.FirstOrDefault(f => f.ServiceId == 90) != null ? CountService.FirstOrDefault(f => f.ServiceId == 90).Quantity : 0,
                            ServiceTDC = CountService.FirstOrDefault(f => f.ServiceId == 91) != null ? CountService.FirstOrDefault(f => f.ServiceId == 91).Quantity : 0
                        };
                    }
                }
                return record;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// GetOfferForCustomer
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <param name="BillId"></param>
        /// <returns></returns>
        public async Task<ServiceCustomer> GetOfferForCustomer(int CustomerId, int BillId)
        {
            try
            {
                //get service used of customer
                List<string> offer = new List<string>(),
                             notOffer = new List<string>(),
                             lastBuyProducts = new List<string>(),
                             lastBuyFMCG = new List<string>(),
                             lastBuyUnderwear = new List<string>();
                string lastTimeBuyProduct = "",
                       lastTimeBuyFMCG = "",
                       lastTimeBuyUnderwear = "",
                       noteOfStaff = "";
                int quantityUseService = 0;
                var getServiceUsed = await CustomerRepo.GetListService(w => w.CustomerId == CustomerId);
                var config = await ConfigRepo.GetList(w => AppConstants.ARR_OFFER_CUSTOMER.Contains(w.Key));
                var customer = await CustomerRepo.GetById(CustomerId);
                var getProductUse = await CustomerRepo.GetListProduct(AppConstants.ARR_CATEGORY.ToList(), CustomerId);
                var getProduct = await CustomerRepo.GetProduct(AppConstants.MAIN_CATEGORY.ToList(), CustomerId);
                var getNoteOfStaff = await CustomerHairAttributeRepo.GetByBillId(BillId);
                if (customer != null)
                {
                    if (customer.TotalBillservice == null || customer.TotalBillservice <= 2)
                    {
                        offer = config.Where(w => AppConstants.OFFER_CUSTOMER.Contains(w.Key)).Select(s => s.Label).ToList();
                    }
                    quantityUseService = customer.TotalBillservice ?? 0;
                }
                if (getProduct != null && getProduct.DateOfBill != null)
                {
                    var time = DateTime.Now.Date - getProduct.DateOfBill;
                    if (time.Days < 25)
                    {
                        notOffer = config.Where(w => AppConstants.NOT_OFFER_CUSTOMER.Contains(w.Key)).Select(s => s.Label).ToList();
                    }
                }

                if (getProductUse.Count > 0)
                {
                    var main = getProductUse.Where(w => AppConstants.MAIN_CATEGORY.Contains(w.CategoryId))
                                            .GroupBy(a => a.BillId, a => a, (key, value) => new { Id = key, List = value.ToList() })
                                            .OrderBy(a => a.Id).FirstOrDefault();
                    if (main != null)
                    {
                        lastTimeBuyProduct = String.Format("{0:dd/MM/yyyy}", main.List.FirstOrDefault().DateOfBill);
                        lastBuyProducts = main.List.Select(s => s.ProductName).ToList();
                    }

                    var fmcg = getProductUse.Where(w => AppConstants.FMCG_CATEGORY.Contains(w.CategoryId))
                                            .GroupBy(a => a.BillId, a => a, (key, value) => new { Id = key, List = value.ToList() })
                                            .OrderBy(a => a.Id).FirstOrDefault();

                    if (fmcg != null)
                    {
                        lastTimeBuyFMCG = String.Format("{0:dd/MM/yyyy}", fmcg.List.FirstOrDefault().DateOfBill);
                        lastBuyFMCG = fmcg.List.Select(s => s.ProductName).ToList();
                    }

                    var underwear = getProductUse.Where(w => AppConstants.UNDERWARE_CATEGORY.Contains(w.CategoryId))
                                            .GroupBy(a => a.BillId, a => a, (key, value) => new { Id = key, List = value.ToList() })
                                            .OrderBy(a => a.Id).FirstOrDefault();

                    if (underwear != null)
                    {
                        lastTimeBuyUnderwear = String.Format("{0:dd/MM/yyyy}", underwear.List.FirstOrDefault().DateOfBill);
                        lastBuyUnderwear = underwear.List.Select(s => s.ProductName).ToList();
                    }
                }

                var lastTimeUseHairCurling = getServiceUsed.FirstOrDefault(w => w.ServiceId == 16 /*uốn*/) != null ?
                                             String.Format("{0:dd/MM/yyyy}", getServiceUsed.FirstOrDefault(w => w.ServiceId == 16).DateOfBill) : "";
                var lastTimeUseHairColor = getServiceUsed.FirstOrDefault(w => w.ServiceId == 14 /*nhuộm*/) != null ?
                                           String.Format("{0:dd/MM/yyyy}", getServiceUsed.FirstOrDefault(w => w.ServiceId == 14).DateOfBill) : "";

                if (getNoteOfStaff != null && !String.IsNullOrEmpty(getNoteOfStaff.Note))
                {
                    noteOfStaff = getNoteOfStaff.Note;
                }

                return new ServiceCustomer()
                {
                    Offer = offer,
                    NotOffer = notOffer,
                    QuantityUseService = quantityUseService,
                    LastTimeUseHairCurling = lastTimeUseHairCurling,
                    LastTimeUseHairColor = lastTimeUseHairColor,
                    LastTimeBuyProduct = lastTimeBuyProduct,
                    LastBuyProducts = lastBuyProducts,
                    LastTimeBuyFMCG = lastTimeBuyFMCG,
                    LastBuyFMCG = lastBuyFMCG,
                    LastTimeBuyUnderwear = lastTimeBuyUnderwear,
                    LastBuyUnderwear = lastBuyUnderwear,
                    NoteOfStaff = noteOfStaff
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get bill info
        /// </summary>
        /// <param name="billId"></param>
        /// <param name="stylistId"></param>
        /// <returns></returns>
        public async Task<Response> GetBillInfo(int billId, int stylistId)
        {
            try
            {
                var data = new Response();
                // get data
                var getData = await BillServiceRepo.GetDbRead(a => a.Id == billId
                                                        && a.StaffHairdresserId == stylistId
                                                        && a.Pending == 0
                                                        && a.IsDelete == 0
                                                        && a.ServiceIds != null);
                if (getData != null)
                {
                    data = new Response
                    {
                        Status = true,
                        Message = "Lấy dữ liệu thành công",
                        Data = new BillInfo
                        {
                            CustomerId = getData.CustomerId.Value,
                            IsImage = getData.Images == null ? false : true,
                            TimeInBill = new DateTime(getData.CreatedDate.Value.Year,
                                                              getData.CreatedDate.Value.Month,
                                                              getData.CreatedDate.Value.Day,
                                                              getData.CreatedDate.Value.Hour,
                                                              getData.CreatedDate.Value.Minute,
                                                              getData.CreatedDate.Value.Second,
                                                              getData.CreatedDate.Value.Millisecond),
                            CompleteTimeBill = new DateTime(getData.CompleteBillTime.Value.Year,
                                                              getData.CompleteBillTime.Value.Month,
                                                              getData.CompleteBillTime.Value.Day,
                                                              getData.CompleteBillTime.Value.Hour,
                                                              getData.CompleteBillTime.Value.Minute,
                                                              getData.CompleteBillTime.Value.Second,
                                                              getData.CompleteBillTime.Value.Millisecond)
                        }

                    };
                }
                else
                {
                    data = new Response
                    {
                        Status = false,
                        Message = "Không có dữ liệu"
                    };

                }
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        /// <summary>
        /// Get detail Bill info
        /// </summary>
        public async Task<DetailBillPendingStatus> GetDetailBill(int billId)
        {
            try
            {
                var bill = await BillServiceRepo.GetDetailBillPendingStatus(billId);
                if (bill != null)
                {
                    bill.SkinnerBoundTime = "(*) Mỗi 12' nhân viên mới được nhập mã 1 lần!";
                    if (bill.StylistId == null) bill.StylistId = 0;
                    if (bill.SkinnerId == null) bill.SkinnerId = 0;
                }
                return bill;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Get all bill pending of salon today
        /// </summary>
        public async Task<List<BillPendingItem>> GetAllBillPending(Staff staff)
        {
            try
            {
                DateTime timeFrom = DateTime.Today; // Format 20/02/2019 12:00:00

                int salonId = 0;
                // Get salon that staff has been attendance
                var timeKeeping = await FlowTimeKeepingRepo.Get(w => w.IsDelete != 1 && w.WorkDate == timeFrom && w.StaffId == staff.Id && w.IsEnroll == true);

                if (timeKeeping != null && timeKeeping.SalonId > 0)
                    salonId = timeKeeping.SalonId;
                else
                    salonId = staff.SalonId != null ? (int)staff.SalonId : 0;

                if (salonId <= 0)
                {
                    return new List<BillPendingItem> { };
                }

                // check staff permission, if root or admin then get bill of all salon
                if (Array.IndexOf(new string[] { "root", "admin" }, staff.Permission) != -1)
                {
                    salonId = 0;
                }

                // get all bill of a salon today, if salonId = 0 then get all salon
                var list = await BillServiceRepo.GetAllBillPending(staff.Id, salonId, timeFrom);
                return list;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Get all bill pending of a staff in salon today
        /// </summary>
        public async Task<List<BillPendingItem>> GetAllBillPendingOfStaff(Staff staff)
        {
            try
            {
                DateTime timeFrom = DateTime.Today; // Format 20/02/2019 12:00:00

                int salonId = 0;
                // Get salon that staff has been attendance
                var timeKeeping = await FlowTimeKeepingRepo.Get(w => w.IsDelete != 1 && w.WorkDate == timeFrom && w.StaffId == staff.Id && w.IsEnroll == true);

                if (timeKeeping != null && timeKeeping.SalonId > 0)
                    salonId = timeKeeping.SalonId;
                else
                    salonId = staff.SalonId != null ? (int)staff.SalonId : 0;

                if (salonId <= 0)
                {
                    return new List<BillPendingItem> { };
                }

                var list = new List<BillPendingItem> { };

                if (Array.IndexOf(new string[] { "root", "admin" }, staff.Permission) != -1)
                {
                    // check staff permission, if root or admin then get bill of all salon
                    list = await BillServiceRepo.GetAllBillPending(staff.Id, 0, timeFrom);
                }
                else if (staff.Type == 1)
                {
                    // get all bill pending of a stylist in salon today
                    list = await BillServiceRepo.GetAllBillPendingOfStylist(staff.Id, salonId, timeFrom);
                }
                else if (staff.Type == 2)
                {
                    // get all bill pending of a skinner in salon today
                    list = await BillServiceRepo.GetAllBillPendingOfSkinner(staff.Id, salonId, timeFrom);
                }

                return list;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
