﻿using write = APIStaff.Models.Solution_30Shine;
using APIStaff.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIStaff.Service
{
    public class Customer
    {
        private readonly ICustomerHairAttributeRepo CustomerHairAttributeRepo;
        public Customer(ICustomerHairAttributeRepo customerHairAttributeRepo)
        {
            CustomerHairAttributeRepo = customerHairAttributeRepo;
        }

        /// <summary>
        /// add or update sale for app staff
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <param name="BillId"></param>
        /// <param name="Note"></param>
        /// <returns></returns>
        public async Task AddOrUpdate(int UserId, int CustomerId, int BillId, string Note)
        {
            var record = await CustomerHairAttributeRepo.GetByBillId(BillId);
            if (record != null)
            {
                record.SessionId = UserId;
                record.Note = Note;
                record.ModifiedTime = DateTime.Now;
                CustomerHairAttributeRepo.Update(record);
            }
            else
            {
                record = new write.CustomerHairAttribute()
                {
                    SessionId = UserId,
                    CustomerId = CustomerId,
                    BillServiceId = BillId,
                    Note = Note,
                    CreatedTime = DateTime.Now,
                    IsDelete = false
                };
                CustomerHairAttributeRepo.Add(record);
            }
            await CustomerHairAttributeRepo.SaveChangeAsync();
        }
    }
}
