﻿using APIStaff.Models.Solution_30Shine;
using APIStaff.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIStaff.Service
{
    public class CheckinCheckoutService
    {
        private readonly ICheckinCheckoutRepo CheckinCheckoutRepo;
        private readonly IFlowTimeKeepingRepo FlowTimeKeepingRepo;

        public CheckinCheckoutService(ICheckinCheckoutRepo checkinCheckoutRepo, IFlowTimeKeepingRepo flowTimeKeepingRepo)
        {
            CheckinCheckoutRepo = checkinCheckoutRepo;
            FlowTimeKeepingRepo = flowTimeKeepingRepo;
        }


        public async Task AddCheckinTime(CheckinCheckout newRecord, FlowTimeKeeping flowTimeKeeping, TimeSpan? startTime)
        {
            try
            {
                var oldRecord = await CheckinCheckoutRepo.Get(a => a.StaffId == newRecord.StaffId && a.WorkDate == newRecord.WorkDate && a.IsDelete == false);
                if (oldRecord != null && oldRecord.Id > 0)
                {
                    oldRecord.IsDelete = true;
                    CheckinCheckoutRepo.Update(oldRecord);

                    newRecord.IsComeLate = oldRecord.IsComeLate;
                }
                else
                {
                    DateTime checkinTime = (DateTime)newRecord.CheckinTime;
                    newRecord.IsComeLate = startTime != null ? TimeSpan.Compare(checkinTime.TimeOfDay, (TimeSpan)startTime) > 0 : false;
                    if (flowTimeKeeping.CheckinFirstTime == null || flowTimeKeeping.CheckinFirstTime.ToString() == "")
                    {
                        flowTimeKeeping.CheckinFirstTime = newRecord.CheckinTime;
                        flowTimeKeeping.IsEnroll = true;
                        FlowTimeKeepingRepo.Update(flowTimeKeeping);
                        await FlowTimeKeepingRepo.SaveChanges();
                    }
                }
                CheckinCheckoutRepo.Add(newRecord);
                await CheckinCheckoutRepo.SaveChanges();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task AddCheckoutTime(int staffId, int salonId, DateTime checkoutTime, FlowTimeKeeping flowTimeKeeping, TimeSpan? endTime)
        {
            try
            {
                var oldRecord = await CheckinCheckoutRepo.Get(a => a.StaffId == staffId && a.SalonId == salonId && a.WorkDate == checkoutTime.Date && a.IsDelete == false);
                if (oldRecord != null && oldRecord.Id > 0 && oldRecord.CheckinTime != null)
                {
                    oldRecord.CheckoutTime = checkoutTime;
                    oldRecord.IsLeaveEarly = endTime != null ? TimeSpan.Compare(checkoutTime.TimeOfDay, (TimeSpan)endTime) < 0 : false;
                    CheckinCheckoutRepo.Update(oldRecord);
                    await CheckinCheckoutRepo.SaveChanges();

                    if (flowTimeKeeping.CheckoutLastTime == null || flowTimeKeeping.CheckoutLastTime.ToString() == "")
                    {
                        flowTimeKeeping.CheckoutLastTime = oldRecord.CheckoutTime;
                        FlowTimeKeepingRepo.Update(flowTimeKeeping);
                        await FlowTimeKeepingRepo.SaveChanges();
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
