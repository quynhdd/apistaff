﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIStaff.Extentions
{
    public interface IPushNotice
    {
        void PushErrorToSlack(string className, string methodName, string messageError);
        void PushInfoToSlack(string message);

    }
}
