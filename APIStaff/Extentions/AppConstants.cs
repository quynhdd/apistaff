﻿namespace APIStaff.Extentions
{
    public class AppConstants
    {
        public static string APP_ID = "myApp001";
        public static string API_ID = "myAPI001";
        public readonly string[] GROUP_OR_USERNAME = { "sys", "duycs", "admin", "slm", "gsm", "gs" };
        public readonly string[] PERMISSTIONS_BASIC = { "fullControl", "modify", "readExcute", "read", "listContents", "write", "special" };
        public readonly string[] RESOURCE_TABLE = { "table1", "table2", "table3" };
        public readonly string[] RESOURCE_API = { "api1", "api2", "api3" };
        public readonly string[] ACTION = { "add", "remove", "edit", "view" };
        public const string ACTION_READ = "read";
        public const string ACTION_WRITE = "write";
        public const string ACTION_EDIT = "edit";
        public const string ACTION_VIEW = "view";
        public const string ACTION_DEL = "del";
        //culture info
        public const string LANGUAGE_CULTURE_NAME = "vi-VN";
        //IsAccountLogin
        public const string IsAccountLogin = "1";
        public const int Type = 1;
        // Supper Pass
        public const string SUPPER_PASS = "4hg@EaiUJsdfx3R*24vPW%n7!Qu1~rFARHK#0wz&D!%HAN";
        public const string SUPPER_TOKEN = "SUPPERA45&iOiJKV1Q!@*&$iOyX2lkIjoyOSwidXNlcm5hbWUiOiJ0dWF43afG&!&uZG10ZXN0IiwiZXhwIjoxNTU3ODAjkasdfh72134#$%SDAF&dsFpbCI6InR1YW5kbUB0asd@!#dfRT56#@$WdfaWF0IjoxNTU3NzIyMMK4";

        //notify
        public const bool IS_LOG = false;
        public const string GROUP_SLACK = "CBHB99UGM";
        public const string API_PUSH_NOTIC_TO_SLACK = "https://api-push-notic.30shine.com/api/pushNotice/slack";
        public const string TEAM = "api";
        public const string MODULE_NAME = "30Shine\\API-ManageApp";
        public const string TYPE_ERROR = "Error";
        public const string TYPE_INFO = "Info";
        public const string TYPE_WARNING = "Warning";
        //date
        public const string DATE_FORMAT = "dd-MM-yyyy";

        //config customer
        public static string[] ARR_OFFER_CUSTOMER = { "offer_customer", "not_offer_customer" };
        public static string[] OFFER_CUSTOMER = { "offer_customer" };
        public static string[] NOT_OFFER_CUSTOMER = { "not_offer_customer" };

        public static int[] ARR_CATEGORY = { 6, 7, 53, 54, 93, 94, 57, 81, 82, 56, 74, 75, 76, 77, 78, 79, 80, 59, 86, 87, 88, 89, 55, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 99 };
        public static int[] MAIN_CATEGORY = { 6, 7, 53, 54, 93, 94 };
        public static int[] FMCG_CATEGORY = { 57, 81, 82, 56, 74, 75, 76, 77, 78, 79, 80, 59, 86, 87, 88, 89, 55, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73 };
        public static int[] UNDERWARE_CATEGORY = { 99 };

        // stylemaster
        public static int STYLE_MASTER_APPROVE_STATUS = 3;

        public class OrderStatus
        {
            //
            public const int NOT_ORDER = 1;
            public const int ORDERED = 2;
            public const int ORDER_REJECT = 3;
            public const int WAIT_IMPORT = 4;
            public const int IMPORTED = 5;
        }
        public class CosmeticType
        {
            public const int CosmeticOrder = 1;
            public const int ServiceOrder = 2;
        }

        public class StatusFacilities
        {
            public const int StatusCheck = 2;
            public const int StatusHandling = 3;
            public const int LevePriority = 1;
        }
    }
}
