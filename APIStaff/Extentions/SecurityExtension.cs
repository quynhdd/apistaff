﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace APIStaff.Extentions
{
    public class SecurityExtension
    {
        public static string GenPassword(string input)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                var scrPush = "uaefsfkoiu&@(^%=";
                byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input + scrPush));
                StringBuilder sBuilder = new StringBuilder();

                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }

                return sBuilder.ToString();
            }
        }
    }
}
