﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace APIStaff.Extentions
{
    public class Common
    {
        /// <summary>
        /// Gen code 
        /// </summary>
        /// <param name="number"></param>
        /// <param name="prefix"></param>
        /// <param name="Date"></param>
        /// <returns></returns>
        public static string GenCode(int number, string prefix, DateTime Date)
        {
            return prefix + String.Format("{0:ddMMyy}", Date) + number;
        }
    }
}
