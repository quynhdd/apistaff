﻿using Amazon.CognitoIdentityProvider;
using Amazon.CognitoIdentityProvider.Model;
using Amazon.Extensions.CognitoAuthentication;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace APIStaff.Extentions
{
    public class CognitoExtension : ICognitoExtension
    {

        private readonly Amazon.RegionEndpoint Region;
        private readonly AmazonCognitoIdentityProviderClient providerClient;

        private IConfiguration Configuration;

        public CognitoExtension(IConfiguration configuration)
        {
            Configuration = configuration;
            Region = Amazon.RegionEndpoint.APSoutheast1;
            providerClient =
                    new AmazonCognitoIdentityProviderClient(
                        Configuration.GetSection("Cognito:ACCESS_KEY").Value,
                        Configuration.GetSection("Cognito:SECRET_KEY").Value,
                        Region);
        }

        /// <summary>
        /// Registers the specified <paramref name="username"/> in Cognito with the given password,
        /// as an asynchronous operation. Also submits the validation data to the pre sign-up lambda trigger.
        /// </summary>
        public void SignUp(string username, string password)
        {
            try
            {
                SignUpRequest signUpRequest = new SignUpRequest()
                {
                    ClientId = Configuration.GetSection("Cognito:APP_CLIENT_ID").Value,
                    Username = username,
                    Password = password
                };

                SignUpResponse signUpResponse = providerClient.SignUpAsync(signUpRequest).Result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Admin confirms the specified <paramref name="user"/>, regardless of the confirmation code
        /// as an asynchronous operation.
        /// </summary>
        /// <param name="user">The user to confirm.</param>
        /// <returns>
        /// The <see cref="Task"/> that represents the asynchronous operation, containing the <see cref="IdentityResult"/>
        /// of the operation.
        /// </returns>
        public void AdminConfirmSignUp(string username)
        {
            try
            {
                AdminConfirmSignUpRequest adminConfirmSignUpRequest = new AdminConfirmSignUpRequest()
                {
                    Username = username,
                    UserPoolId = Configuration.GetSection("Cognito:POOL_ID").Value
                };
                AdminConfirmSignUpResponse adminConfirmSignUpResponse = providerClient.AdminConfirmSignUpAsync(adminConfirmSignUpRequest).Result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Get access token when user login
        /// </summary>
        /// <param name="username">The user name of the user</param>
        /// <param name="password">The current password of the user.</param>
        /// <returns></returns>
        public AuthFlowResponse GetUserTokenForLogin(string username, string password)
        {
            //Create CognitoUserPool and CognitoUser objects. Call the StartWithSrpAuthAsync method with an InitiateSrpAuthRequest that contains the user password.
            try
            {
                //get user pool of cognito
                CognitoUserPool cognitoUserPool = new CognitoUserPool(
                    Configuration.GetSection("Cognito:POOL_ID").Value,
                    Configuration.GetSection("Cognito:APP_CLIENT_ID").Value,
                    providerClient);

                //get user in user pool
                CognitoUser cognitoUser = new CognitoUser(username, Configuration.GetSection("Cognito:APP_CLIENT_ID").Value, cognitoUserPool, providerClient);

                //password request syntax
                InitiateSrpAuthRequest authRequest = new InitiateSrpAuthRequest()
                {
                    Password = password
                };

                //Call the StartWithSrpAuthAsync method with an InitiateSrpAuthRequest that contains the user password.
                AuthFlowResponse authResponse = cognitoUser.StartWithSrpAuthAsync(authRequest).Result;
                return authResponse;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public string GetUsernameFromToken(string accessToken)
        {
            var payload = Base64UrlEncoder.Decode(accessToken.Split(".")[1]);
            var payloadJson = JsonConvert.DeserializeObject<Payload>(payload);
            return payloadJson.username;
        }

        private class Payload
        {
            public string username { get; set; }
        }


        /// <summary>
        /// Changes the password on the cognito account associated with the <paramref name="accessToken"/>.
        /// </summary>
        /// <param name="accessToken">The access token of the user.</param>
        /// <param name="oldPassword">The current password of the user.</param>
        /// <param name="newPassword">The new passord for the user.</param>
        /// The that represents the asynchronous operation, containing the
        /// of the operation.
        public void ChangePassword(string accessToken, string oldPassword, string newPassword)
        {
            try
            {
                ChangePasswordRequest changePasswordRequest = new ChangePasswordRequest()
                {
                    AccessToken = accessToken,
                    PreviousPassword = oldPassword,
                    ProposedPassword = newPassword
                };
                ChangePasswordResponse changePasswordResponse = providerClient.ChangePasswordAsync(changePasswordRequest).Result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// refresh token
        /// </summary>
        public async Task<string> RefreshToken(string username, string idToken, string accessToken, string refreshToken)
        {
            try
            {
                //get user pool of cognito
                CognitoUserPool cognitoUserPool = new CognitoUserPool(
                    Configuration.GetSection("Cognito:POOL_ID").Value,
                    Configuration.GetSection("Cognito:APP_CLIENT_ID").Value,
                    providerClient);

                var user = cognitoUserPool.GetUser(username);

                user.SessionTokens = new CognitoUserSession(idToken, accessToken, refreshToken, DateTime.Now, DateTime.Now.AddMinutes(30 * 24 * 60));
                // refresh token
                var authResponse = await user.StartWithRefreshTokenAuthAsync(new InitiateRefreshTokenAuthRequest()
                {
                    AuthFlowType = AuthFlowType.REFRESH_TOKEN
                }).ConfigureAwait(false);

                return authResponse.AuthenticationResult?.AccessToken;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
