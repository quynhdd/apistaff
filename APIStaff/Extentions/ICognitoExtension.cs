﻿
using Amazon.Extensions.CognitoAuthentication;
using System.Threading.Tasks;

namespace APIStaff.Extentions
{
    public interface ICognitoExtension
    {
        void SignUp(string username, string password);
        void AdminConfirmSignUp(string username);
        AuthFlowResponse GetUserTokenForLogin(string username, string password);
        void ChangePassword(string token, string oldPassword, string newPassword);
        string GetUsernameFromToken(string accessToken);
        Task<string> RefreshToken(string username, string idToken, string accessToken, string refreshToken);
    }
}
