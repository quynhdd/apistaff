﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using write = APIStaff.Models.Solution_30Shine;
using read = APIStaff.Models.Solution_30Shine_BI;
using Microsoft.EntityFrameworkCore;
using APIStaff.Extentions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.PlatformAbstractions;
using System.IO;
using APIStaff.Repository.Interface;
using APIStaff.Repository.Implement;
using APIStaff.Service;
using Newtonsoft.Json.Serialization;
using Amazon;
using Amazon.DynamoDBv2;
using APIStaff.Models.Dynamo;

namespace APIStaff
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                            .SetBasePath(env.ContentRootPath)
                            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                            .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                            .AddEnvironmentVariables();
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<write.Solution_30shineContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("Solution_30shineContext")));
            services.AddDbContext<read.Solution_30shineContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("Solution_30shineContext_BI")));
            //add interface, implement
            services.AddSingleton<IPushNotice, PushNotice>();
            services.AddTransient<IBillServiceRepo, BillServiceRepo>();
            services.AddTransient<IBillServiceHisRepo, BillServiceHisRepo>();
            services.AddTransient<IFlowServiceRepo, FlowServiceRepo>();
            services.AddTransient<ICustomerRepo, CustomerRepo>();
            services.AddTransient<IConfigRepo, ConfigRepo>();
            services.AddTransient<ICustomerHairAttributeRepo, CustomerHairAttributeRepo>();
            services.AddTransient<IStyleMasterRepo, StyleMasterRepo>();
            services.AddTransient<IStyleMasterLogRepo, StyleMasterLogRepo>();
            services.AddTransient<IStaffRepo, StaffRepo>();
            services.AddTransient<IStaffBookRepo, StaffBookRepo>();
            services.AddTransient<IHairStyleRepo, HairStyleRepo>();
            services.AddTransient<ICheckinCheckoutRepo, CheckinCheckoutRepo>();
            services.AddTransient<IDeviceRepo, DeviceRepo>();
            services.AddTransient<IFlowTimeKeepingRepo, FlowTimeKeepingRepo>();
            services.AddTransient<IWorkTimeRepo, WorkTimeRepo>();
            services.AddTransient<BillService>();
            services.AddTransient<Customer>();
            services.AddTransient<CheckinCheckoutService>();
            services.AddSingleton<ICognitoExtension, CognitoExtension>();
            services.AddTransient<IProductRepo, ProductRepo>();
            services.AddTransient<IServiceRepo, ServiceRepo>();
            services.AddTransient<IFavoriteProductServiceRepo, FavoriteProductServiceRepo>();

            //generate service
            // AWS Options
            var awsOptions = Configuration.GetAWSOptions();
            Environment.SetEnvironmentVariable("AWS_ACCESS_KEY_ID", Configuration["AWS:AccessKey"]);
            Environment.SetEnvironmentVariable("AWS_SECRET_ACCESS_KEY", Configuration["AWS:SecretKey"]);
            Environment.SetEnvironmentVariable("AWS_REGION", Configuration["AWS:Region"]);

            services.AddDefaultAWSOptions(awsOptions);
            awsOptions.Region = RegionEndpoint.APSoutheast1;

            var client = awsOptions.CreateServiceClient<IAmazonDynamoDB>();

            var dynamoDbOptions = new DynamoDbOptions();
            ConfigurationBinder.Bind(Configuration.GetSection("DynamoDbTables"), dynamoDbOptions);


            // This is where the magic happens
            services.AddScoped<IDynamoDbContext<SurveyQuestion>>(provider => new DynamoDbContext<SurveyQuestion>(client, dynamoDbOptions.SurveyQuestion));
            services.AddScoped<IDynamoDbContext<SurveyAnswer>>(provider => new DynamoDbContext<SurveyAnswer>(client, dynamoDbOptions.SurveyAnswer));
            services.AddScoped<IDynamoDbContext<SurveyUser>>(provider => new DynamoDbContext<SurveyUser>(client, dynamoDbOptions.SurveyUser));
            services.AddScoped<IDynamoDbContext<SurveyQuestionType>>(provider => new DynamoDbContext<SurveyQuestionType>(client, dynamoDbOptions.SurveyQuestionType));

            //auth service
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Configuration["Jwt:Issuer"],
                        ValidAudience = Configuration["Jwt:Issuer"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]))
                    };
                });
            services.AddMvc();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    TermsOfService = "None",
                });
                c.IgnoreObsoleteActions();
                c.IgnoreObsoleteProperties();
                c.CustomSchemaIds((type) => type.FullName);
                try
                {
                    var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                    var xmlPath = Path.Combine(basePath, "APIStaff.xml");
                    c.IncludeXmlComments(xmlPath);
                }
                catch { }
            });
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy", builder =>
                {
                    builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                });
            });
            services.AddDirectoryBrowser();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddFile("Logs/myapp-{Date}.txt");
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            // Enable use files
            app.UseFileServer();
            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseCors(builder =>
            {
                builder.AllowAnyHeader();
                builder.AllowAnyMethod();
                builder.AllowCredentials();
                builder.AllowAnyOrigin();
                //corsBuilder.WithOrigins("http://localhost:51060/"); // for a specific url.
            });
            //use auth
            app.UseAuthentication();
            app.UseMvc();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "API V2");
                c.InjectStylesheet("/swagger/ui/custome.css");
                c.InjectStylesheet("/lib/semantic-ui/dist/semantic.css");
                c.InjectJavaScript("/lib/semantic-ui/dist/semantic.js");
                c.InjectJavaScript("/swagger/authen/custome.js");
                c.InjectJavaScript("/swagger/authen/basic-auth.js");
                c.RoutePrefix = "staff";

            });
            app.UseCors("CorsPolicy");
            app.UseAuthentication();
        }
    }
}
