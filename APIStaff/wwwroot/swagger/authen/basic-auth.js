﻿(function () {

    //init view
    var wrapContainer = $("#resources_container");
    var tokenUi = '<div class="input">' +
        '<h2>Authenticate</h2>' +
        '<div class="ui form"><div class="fields">' +

        '<div class="seven wide field">' +
        '<label>User Account</label>' +
        '<div class="field"><input class="text" placeholder="Account" id="input_account" value="oanh" type="text"></div>' +
        '</div>' +

        '<div class="seven wide field">' +
        '<label>User Password</label>' +
        '<div class="field"><input class="text" placeholder="Password" id="input_password" name="password" type="password" value="oanh1992" size="25"></div>' +
        '</div>' +

        '<div class="seven wide field">' +
        '<label>DeviceId</label>' +
        '<div class="field"><input class="text" placeholder="Device Id" id="input_device_id" name="device id" value="abcd"  type="text"></div>' +
        '</div>' +

        '<div class="seven wide field">' +
        '<label>IP</label>' +
        '<div class="field"><input class="text" placeholder="Device Id" id="input_ip" name="ip" value="abcd"  type="text"></div>' +
        '</div>' +

        '<div class="seven wide field">' +
        '<label>App API Id</label>' +
        '<div class="field"><input class="text" placeholder="App Id" id="input_app_api_id" name="app id" value="00d60ec9-44c8-48d2-9451-85ea1624427d"  type="text"></div>' +
        '</div>' +

        '<div class="seven wide field">' +
        '<label>App Client Version</label>' +
        '<div class="field"><input class="text" placeholder="App Id" id="input_app_client_version" name="app id" value="2"  type="text"></div>' +
        '</div>' +

        '<div class="two wide field">' +
        '</div></div></div>' +

        '<div class="two wide field">' +
        '<div class="ui primary button" id="input_authenticate" name="authenticate" value="Get token">Get access token</div>' +
        '<div>' +

        '<div class="ui form" style="margin-top:10px">' +
        '<div class="">' +
        '<div class="wide field">' +
        '<label>Token key</label>' +
        '<div class="ui input"><input class="text" placeholder="Tokenkey" id="token_key" type="text" size="25" value=""></div>' +
        '</div>' +
        '</div></div>' +


        //'<label>Application</label>' +
        //'<div class="field">' +
        //'<select class="ui fluid search dropdown select-app" name="app" value="">' +
        //'<option class="app" id="option-nothing" value="">Choose an App</option>' +
        //'</select>' +
        '</div>' +

        '</div>' +
        '</div>';

    $(tokenUi).insertBefore(wrapContainer);

    //init val
    var apiApplication = "/api/application/all";
    var apiLogin = "/api/user/login";
    var applicationUid = '00d60ec9-44c8-48d2-9451-85ea1624427d';

    //btn
    var inputAuth = $("#input_authenticate");
    var btnOption = $("option.app");
    var optionNothing = $('#option-nothing');
    var inputTokenKey = $('#token_key');

    //bind elm app
    //bindApplication();

    inputAuth.click(function () {
        var account = $("#input_account").val();
        var password = $("#input_password").val();
        var customerPhone = $("#input_customer_phone").val();
        var deviceId = $("#input_device_id").val();
        var ip = $("#input_ip").val();
        var appApiId = $("#input_app_api_id").val();
        var appClientVersion = $("#input_app_client_version").val();

        getToken(account, password, deviceId, ip, appApiId, appClientVersion);
        addAuthorization();
    });

    //bind app list
    function bindApplication() {
        //let request = { Authorization: token };
        $.ajax({
            contentType: "application/json",
            type: "get",
            url: apiApplication,
            dataType: "json",
            //async: false,
            //headers: request,
            success: function (data) {
                var items = "";
                if (data && data.length > 0) {
                    $.each(data, function (index, value) {
                        console.log(value);
                        items += '<option class="app" value="' + value.uid + '">' + value.name + '</option>';
                    });
                    optionNothing.empty().after(items);

                    //call back
                    var optionApp = $(".app");
                    var selectApp = $(".select-app");

                    optionApp.click(function () {
                        console.log("click choose app");
                        let value = $(this).attr("value");
                        console.log(value);
                        applicationUid = value;
                        selectApp.attr("value", value);
                    });
                }
            },
            error: function (jqXhr, textStatus, errorMessager) {
                console.log(errorMessager + "\n" + jqXhr.responseJSON);
                alert(errorMessager + "\n" + jqXhr.responseJSON);
            }
        });
    };

    //get token
    function getToken(account, password, deviceId, ip, appApiId, appClientVersion) {
        let client = detectClient();
        var request = { account: account, Password: password, deviceId: deviceId, ip: ip, appId: appApiId, appVersion: appClientVersion };
        //console.log(request);
        console.log('Detecting browser :' + client);
        $.ajax({
            contentType: "application/json",
            type: "post",
            url: apiLogin,
            dataType: "json",
            headers: request,
            //data: request,
            async: false,
            success: function (result) {
                console.log(result);
                var accessToken = result.accessToken;
                if (accessToken) {
                    inputTokenKey.val("Bearer " + accessToken);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(errorThrown);
            }
        });
    };

    function addAuthorization() {
        let key2 = inputTokenKey.val();
        console.log(key2);
        window.swaggerUi.api.clientAuthorizations.add("key", new SwaggerClient.ApiKeyAuthorization("Authorization", key2, "header"));
    };

    function detectClient() {
        // Opera 8.0+
        var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;

        // Firefox 1.0+
        var isFirefox = typeof InstallTrigger !== 'undefined';

        // Safari 3.0+ "[object HTMLElementConstructor]" 
        var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || safari.pushNotification);

        // Internet Explorer 6-11
        var isIE = /*@cc_on!@*/false || !!document.documentMode;

        // Edge 20+
        var isEdge = !isIE && !!window.StyleMedia;

        // Chrome 1+
        var isChrome = !!window.chrome && !!window.chrome.webstore;

        // Blink engine detection
        var isBlink = (isChrome || isOpera) && !!window.CSS;

        var clientType = "";
        if (isOpera)
            clientType = "Opera";
        if (isFirefox)
            clientType = "Firefox";
        if (isSafari)
            clientType = "Safari";
        if (isIE)
            clientType = "IE";
        if (isEdge)
            clientType = "Edge";
        if (isChrome)
            clientType = "Chrome";
        //if (isBlink)
        //    clientType = "idBlink";

        return clientType;
    }
})();